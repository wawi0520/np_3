/**
    This declares data structure and function related to client/server
    @file    CommonTasks.h
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.24
*/

#ifndef _COMMON_TASKS_H_
#define _COMMON_TASKS_H_
#include "unp.h"
#include "CommonType.h"
#include "Config.h"
#include "NpType.h"

void DebugPacketData( Packet *packetData );

void InitPacketData( Packet *packetData );

void BuildPacketData
    (
    Packet *packetData,
    char *message,
    char *IPAddr,
    const NpInt32 port,
    const NpInt32 flag
    );

void ParsePacketData
    (
    char *message,
    char *IPAddr,
    NpInt32 *port,
    NpInt32 *flag,
    Packet *packetData
    );


/**
    Send requests or replies
    @param [in] sockFd The socket descriptor for write
    @param [in] IPAddr The canonical IP address for the destination
                            node, in presentation format
    @param [in] port The destination port number
    @param [in] msg The message to be sent
    @param [in] flag Set to force a route rediscovery anyway.
*/
void msg_send
    (
    const NpInt32 sockFd,
    char *IPAddr,
    const NpInt32 port,
    char *msg,
    const NpInt32 flag
    );

/**
    Send requests or replies
    @param [in] sockFd The socket descriptor for read
    @param [in/out] msg The message received
    @param [in/out] IPAddr The canonical IP address for the source
                            node, in presentation format
    @param [in/out] port The source port number
*/
void msg_recv
    (
    const NpInt32 sockFd,
    char *msg,
    char *IPAddr,
    NpInt32 *port
    );

void IPAddrToHostName( char *hostname, char *IPAddr );

/** Get current time stamp (in second)
    @return the time stamp
*/
NpInt32 GetTimeStamp();

/** Wrapper function for gethostname
    @param [out] hostName A pointer to buffer that returns the host name
    @param [in] len The length of buffer pointed by the hostName
*/
void GetHostName( char* hostName, const NpUint32 len );

/**
    Wrapper function for select
*/
NpInt32
MySelect
    (
    NpInt32 nfds,
    fd_set *readfds,
    fd_set *writefds,
    fd_set *exceptfds,
    struct timeval *timeout
    );


#endif
