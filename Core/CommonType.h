/**
    This declares the packet that mainly for client/server
    @file    CommonType.h
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.22
*/

#ifndef _COMMON_TYPE_H_
#define _COMMON_TYPE_H_

#include "NpType.h"
#include "Config.h"

/**
    Define the structure of Packet for ODR-application communication
    This packet will be packed into AppMsg packet.
*/
typedef struct
{
   char message[MSG_SIZE]; // the message to be sent
   char IPAddr[IP_SIZE];   // the IP address in presentation format
   NpInt32 port;            // the port number
   NpInt32 flag;            // set to force a route rediscovery anyway
} Packet;


#endif
