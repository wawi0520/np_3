/**
    This declares function outputing communication history
    @file    MsgHandler.h
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.24
*/

#ifndef _MSG_HANDLER_H_
#define _MSG_HANDLER_H_

#include "NpType.h"
#include "OdrPacket.h"

void PrintClientSendRequestMsg( char * fromNode, char * toNode );

void PrintClientReceiveResponseMsg
    (
    char *atNode,
    char *fromNode,
    char *message
    );

void PrintClientTimeoutMsg( char *atNode, char *fromNode );

void PrintServerSendResponseMsg( char *atNode, char *fromNode );

void PrintOdrSendRreqMsg
    (
    char *atNode,
    char *fromNode,
    char *toNode,
    char *toMACAddr
    );

void PrintOdrSendRrepMsg
    (
    char *atNode,
    char *fromNode,
    char *toNode,
    char *toMACAddr
    );

void PrintOdrSendAppMsgMsg
    (
    char *atNode,
    char *fromNode,
    char *toNode,
    char *toMACAddr,
    char *payload
    );


#endif
