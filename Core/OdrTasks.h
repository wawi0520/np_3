/**
   This declares the struct and functions related to the
   ODR tasks.
   @file    OdrTasks.h
   @author  Wei-Ying Tsai
   @contact wetsai@cs.stonybrook.edu
   @date 2015.11.22
*/

#ifndef _ODR_TASKS_H_
#define _ODR_TASKS_H_

#include <time.h>
#include "unp.h"
#include "NetworkInterface.h"
#include "RoutingTable.h"
#include "OdrPacket.h"
#include "CommonType.h"
#include "Config.h"
#include "NpType.h"

typedef struct
{
    NpBool isValid;
    char sunPath[SUN_PATH_SIZE];
    NpInt32 port;
    NpInt32 timeStamp;
} PathPortTable;


/** Initialize PathPortTable
    @param [in/out] table The table of type PathPortTable
*/
void InitPathPortTable( PathPortTable *table );

/** Add an entry into PathPortTable
    Note we will generate a time stamp in this function.
    @param [in/out] table the table of type PathPortTable
    @param [in] path The sun_path string
    @param [in] port The port number
    @return TRUE if success; FALSE otherwise
*/
NpBool AddEntry2PathPortTable
    (
    PathPortTable *table,
    char *path,
    NpInt32 port
    );

/** Obtain the port if its corresponding sun_path exists
    @param [out] port The port number
    @param [in] table The table of type PathPortTable
    @param [in] path The path to be looked up
    @return TRUE if the port exists; FALSE otherwise
*/
NpBool GetPortFromPathPortTable
    (
    NpInt32 *port,
    PathPortTable *table,
    char *path
    );

/** Obtain the sun_path if its corresponding port exists
    @param [out] path The path to be looked up
    @param [in] table The table of type PathPortTable
    @param [in] port The port number
    @return TRUE if the path exists; FALSE otherwise
*/
NpBool GetPathFromPathPortTable
    (
    char *path,
    PathPortTable *table,
    const NpInt32 port
    );

/** Print the content of PathPortTable
    @param [in/out] table The table of type PathPortTable
*/
void DebugPathPortTable( PathPortTable *table );

/** Flood (broadcast) RREQ packet
    @param [in] sockFd The socket descriptor for ODR communication
    @param [in] srcIPAddr The source IP address of the node initiating
                    the time request.
    @param [in] broadcastId The broadcast ID.
    @param [in] destIPAddr The IP address of the destination node.
    @param [in] numOfHops The number of hops
    @param [in] hasBeenSent Specify if an RREP for this RREQ
                    has been sent
    @param [in] forceRediscovery Set if force rediscovery
    @param [in] ignoredInterfaceIndex The index assoicated with
                    the network interface to which we don't want to
                    send the RREQ.
    @param [in] ifTable The network interface table
    @param [in] numOfInterfaces The number of elements in the
                    network interface table
*/
void FloodRREQ
    (
    const NpInt32 sockFd,
    char *srcIPAddr,
    const NpUint32 broadcastId,
    char *destIPAddr,
    const NpUint32 numOfHops,
    const NpBool hasBeenSent,
    const NpBool forceRediscovery,
    const NpInt32 ignoredInterfaceIndex,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    );

/** Perform tasks when we receive an RREQ packet
    @param [in] odrFd The socket descriptor for ODR communication
    @param [in] packet The received RREQ packet
    @param [in/out] rreqQueue The RREQ queue
    @param [in/out] lastIndexOfRREQQueue The index assoicated
                            with the last entry of the RREQ queue.
    @param [in/out] routingTable The routing table
    @param [in/out] lastIndexOfRoutingTableThe index assoicated with
                            the last entry of the RREQ queue.
    @param [in] srcMACAddress The source MAC address. Note that this
                            is the 'destination IP address' in the routing table
    @param [in]  interfaceIndex The outgoing interface index in the
                            routing table
    @param [in] thisNodeIPAddr The IP address of this node.
    @param [in] ifTable The network interface table
    @param [in] numOfInterfaces The number of elements in the
                    network interface table
*/
void ProcessOdrRREQPacket
    (
    const NpInt32 odrFd,
    OdrRreqPacket *packet,
    RREQQueue *rreqQueue,
    NpInt32 *lastIndexOfRREQQueue,
    RoutingTable *routingTable,
    NpInt32 *lastIndexOfRoutingTable,
    char *srcMACAddr,
    const NpInt32 interfaceIndex,
    char *thisNodeIPAddr,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    );

/** Perform tasks when we receive an RREP packet
    @param [in] odrFd The socket descriptor for ODR communication
    @param [in] globalBroadcastId The broadcast ID.
    @param [in] rrepPacket The received RREP packet
    @param [in/out] rreqQueue The RREP queue
    @param [in/out] lastIndexOfRREQQueue The index assoicated
                            with the last entry of the RREP queue.
    @param [in/out] routingTable The routing table
    @param [in/out] lastIndexOfRoutingTableThe index assoicated with
                            the last entry of the RREQ queue.
    @param [in] srcMACAddress The source MAC address. Note that this
                            is the 'destination MAC address' in the routing table
    @param [in]  interfaceIndex The outgoing interface index in the
                            routing table
    @param [in] thisNodeIPAddr The IP address of this node.
    @param [in/out] appMsgQueue The AppMsg queue
    @param [in/out] lastIndexOfAppMsgQueue The index associated
                    with the last entry in the AppMsg queue.
    @param [in] ifTable The network interface table
    @param [in] numOfInterfaces The number of elements in the
                    network interface table
*/
void ProcessOdrRREPPacket
    (
    const NpInt32 odrFd,
    NpUint32 *globalBroadcastId,
    OdrRrepPacket *rrepPacket,
    RREQQueue *rreqQueue,
    NpInt32 *lastIndexOfRREQQueue,
    RoutingTable *routingTable,
    NpInt32 *lastIndexOfRoutingTable,
    char *srcMACAddr,
    const NpInt32 interfaceIndex,
    char *thisNodeIPAddr,
    OdrAppMsgPacket *appMsgQueue,
    NpInt32 *lastIndexOfAppMsgQueue,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    );

/** Perform tasks when we receive an AppMsg packet
    @param [in] sockFd The socket descriptor for ODR-application
                            communication
    @param [in] odrFd The socket descriptor for ODR communication
    @param [in] appMsgPacket The received AppMsg packet
    @param [in/out] appMsgQueue The AppMsg queue
    @param [in/out] lastIndexOfAppMsgQueue The index associated
                            with the last entry in the AppMsg queue.
    @param [in] globalBroadcastId The broadcast ID.
    @param [in/out] routingTable The routing table
    @param [in/out] lastIndexOfRoutingTableThe index assoicated with
                            the last entry of the RREQ queue.
    @param [in] srcMACAddress The source MAC address. Note that this
                            is the 'destination MAC address' in the routing table
    @param [in]  interfaceIndex The outgoing interface index in the
                            routing table
    @param [in] thisNodeIPAddr The IP address of this node.
    @param [in] ifTable The network interface table
    @param [in] numOfInterfaces The number of elements in the
                    network interface table
    @param [in] ppTable The path-port table
*/
void ProcessOdrAppMsgPacket
    (
    const NpInt32 sockFd,
    const NpInt32 odrFd,
    OdrAppMsgPacket *appMsgPacket,
    OdrAppMsgPacket *appMsgQueue,
    NpInt32 *lastIndexOfAppMsgQueue,
    NpUint32 *globalBroadcastId,
    RoutingTable *routingTable,
    NpInt32 *lastIndexOfRoutingTable,
    char *srcMACAddr,
    const NpInt32 interfaceIndex,
    char *thisNodeIPAddr,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces,
    PathPortTable *ppTable
    );

void RelayRREP
    (
    const NpInt32 sockFd,
    char *destIPAddr,
    char *destMACAddr, // Expect a succinct format
    char *srcIPAddr,
    const NpInt32 interfaceIndex,
    const NpUint32 numOfHops,
    NpBool forceRediscovery,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    );

void DeliverApplicationMessage
    (
    const NpInt32 odrFd,
    char *payload,
    char *destIPAddr,
    const NpInt32 destPort,
    char *destMACAddr,
    char *srcIPAddr,
    const NpInt32 srcPort,
    const NpInt32 interfaceIndex,
    const NpUint32 numOfHops,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    );

/** Get the IP address whose network interface
    name is eth0
*/
char *GetCanonicalIPAddr( NeworkInterfacesTable *table, const NpUint32 count );

/** Send the message to the application (client/server)
    @param [in] sockFd The socket descriptor for ODR-application
                            communication
    @parma [in] buffer The buffer containing the sending data
    @param [in] bufLen The length of the buffer
    @param [in] sa The socket address to which we send data
    @param [in] addrLen the length of the socket address struct.
*/
void SendMsgToApplication
    (
    const NpInt32 sockFd,
    char *buffer,
    const NpInt32 bufLen,
    SA *sa,
    const NpInt32 addrLen
    );

void FlushAppMsgQueue
    (
    const NpInt32 odrFd,
    OdrAppMsgPacket *queue,
    NpInt32 *lastIndexOfAppMsgQueue,
    char *srcIPAddr,
    char *srcMACAddr,
    const NpInt32 interfaceIndex,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    );

void FlushRrepQueue
    (
    const NpInt32 odrFd,
    OdrRrepPacket *queue,
    NpUint32 *count,
    char *srcIPAddr,
    char *srcMACAddr,
    const NpInt32 interfaceIndex,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    );

// Helper functions
/** Add an RREP into queue
    @param [in/out] queue The RREP queue.
    @param [in/out] count The number of RREPs in the queue.
    @param [in] packetToBeAdded The RREP packet that needs adding
            to the queue.
    @note We need to make a COPY of the to-be-added packet
            and add this copy into queue.
*/
void AddRrepPacketToRrepQueue
    (
    OdrRrepPacket *queue,
    NpUint32 *count,
    OdrRrepPacket *packetToBeAdded
    );

/** Remove a RREP from the queue and shrink the queue if applicable.
    @param [in/out] queue The RREP queue.
    @param [in/out] count The number of RREPs in the queue.
    @param [in] index The index assoicated with the RREP which needs
                removing.
    @note We achieve shrinking the queue by moving the last element
            into the empty slot from which we just removed the RREP.
    @note We only shrink the queue if applicable.
*/
void RemoveRrepPacketAndShrinkRrepQueue
    (
    OdrRrepPacket *queue,
    NpUint32 *count,
    const NpUint32 indexRemoved
    );

#endif

