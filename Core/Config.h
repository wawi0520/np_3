/**
    This declares serveral useful config (macro)
    @file    Config.h
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.22
*/

#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "NpType.h"

#define MAX_LINE 128
#define IP_SIZE 32
// The MAC 'presentation' address will look like xx:xx:xx:xx:xx:xx
#define MAC_PRESENTATION_SIZE 32
// The MAC address will contain only hexadecimal value (no ':')
#define MAC_SIZE 6
#define MSG_SIZE 64
#define IF_NAME_SIZE 32
#define ETHER_FRAME_HEADER_SIZE 14

#define MY_PROTOCOL 51414
#define SERVER_PORT 51414
#define SERVER_PATH "/tmp/SERV_wetsai_51414"
#define ODR_PATH "/tmp/ODR_wetsai_51414"
// http://linux.die.net/man/3/mkstemp
// The last six characters of template must be "XXXXXX"
#define TEMPLATE_PATH "/tmp/wetsai_XXXXXX"
#define SUN_PATH_SIZE 128
#define TIME_MESSAGE "TMQ" // TiMe Request

#define MAX_APP_PER_ODR 64
#define MAX_APPMSG_PER_ODR 64
#define MAX_RREQ_PER_ODR 64
#define MAX_RREP_PER_ODR 64
#define MAX_INTERFACE_PER_NODE 16
#define MAX_ENTRY_PER_ROUTING_TABLE 256

#define INVALID_INTERFACE_INDEX (-1)

#endif
