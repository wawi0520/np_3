/**
   This implements the server's main functionality.
   @file    Server.c
   @author  Wei-Ying Tsai
   @contact wetsai@cs.stonybrook.edu
   @date 2015.11.22
*/

#include "unp.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "unp.h"
#include "CommonTasks.h"
#include "MsgHandler.h"
#include "CommonType.h"
#include "Config.h"
#include "NpType.h"

void DebugReceivedMsg
    (
    char *msg,
    char *IPAddr,
    const NpInt32 port
    );

NpInt32 main( NpInt32 argc, char *argv[] )
{
    struct sockaddr_un servaddr;
    bzero( &servaddr, sizeof( servaddr ) );
    struct sockaddr_un cliaddr;

    unlink( SERVER_PATH );

    NpInt32 sockFd = Socket( AF_LOCAL, SOCK_DGRAM, 0 );
    servaddr.sun_family = AF_LOCAL;
    bcopy( SERVER_PATH, servaddr.sun_path, strlen( SERVER_PATH ) );

    Bind( sockFd, (SA *) &servaddr, sizeof( servaddr ) );

    char thisHostNode[MAX_LINE];
    GetHostName( thisHostNode, MAX_LINE );

    for( ; ; )
    {
        NpInt32 port;
        char msg[MAX_LINE];
        char IPAddr[IP_SIZE];
        bzero( &msg, MAX_LINE );
        bzero( &IPAddr, IP_SIZE );

        msg_recv( sockFd, msg, IPAddr, &port );
        if( 0 == bcmp( msg, "Server Time", strlen( "Server Time" ) ) )
        {
            continue;
        }

        //DebugReceivedMsg( msg, IPAddr, port );

        char fromHostNode[MAX_LINE];
        bzero( &fromHostNode, MAX_LINE );
        IPAddrToHostName( fromHostNode, IPAddr );

        // Get current time
        char buffer[MAX_LINE];
        bzero( &buffer, MAX_LINE );
        time_t ticks= time(NULL);
        sprintf( buffer, "Server Time: %.24s", ctime(&ticks) );

        // Send current time
        msg_send( sockFd, IPAddr, port, buffer, 0);
        PrintServerSendResponseMsg( thisHostNode, fromHostNode );
    }

    return 0;
}

void DebugReceivedMsg
    (
    char *msg,
    char *IPAddr,
    const NpInt32 port
    )
{
    printf( "Received from %s(%d): %s\n", IPAddr, port, msg );
}

