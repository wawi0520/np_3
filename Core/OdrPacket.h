/**
    This declares the struct and functions related to the
    ODR packet. Three types of ODR packets are available:
    1) RREQ packet, 2) RREP packet, and 3) AppMsg packet.
    @file    OdrPacket.h
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.22
*/
#ifndef _ODR_PACKET_H_
#define _ODR_PACKET_H_
#include "unp.h"
#include "CommonType.h"
#include "Config.h"
#include "NpType.h"

typedef enum { ODR_RREQ = 0, ODR_RREP, ODR_APPMSG } ODRPacketType;
typedef enum
{
    // No corresponding source IP address is found
    RESULT_RREQ_NOT_FOUND = 0,
    // Find a corresponding source IP address and new broadcast id
    // id is smaller than that in the queue
    RESULT_RREQ_SMALLER_BID,
    // Find a corresponding source IP address and new broadcast id
    // id is larger than that in the queue
    RESULT_RREQ_LARGER_BID,
    // Exactly the same entry is found. That means this is indeed the
    // same RREQ.
    RESULT_RREQ_SAME_BID
} QueryRREQQueueResult;

/** Define the structure of RREQ Packet
*/
typedef struct
{
    ODRPacketType odrType;
    char srcIPAddr[IP_SIZE];
    NpUint32 broadcastId;
    char destIPAddr[IP_SIZE];
    NpUint32 numOfHops;
    NpBool hasBeenSent;
    NpBool forceRediscovery;    // For RREQ and RREP
} OdrRreqPacket;

/** Define the structure of RREP Packet
*/
typedef struct
{
    ODRPacketType odrType;
    char srcIPAddr[IP_SIZE];
    char destIPAddr[IP_SIZE];
    NpUint32 numOfHops;
    NpBool forceRediscovery;    // For RREQ and RREP
} OdrRrepPacket;

/**
    Define a queue for storing 'sent' RREQ.
    Note that the source IP address and the broad cast id
    uniquely identify the ODR message
*/
typedef struct
{
    NpBool valid;
    int broadcastId;
    char srcIPAddr[IP_SIZE];
} RREQQueue;

/** Define the structure of AppMsg Packet
    This ODR packet contains infromation from the application
    (server/client).
*/
typedef struct
{
    ODRPacketType odrType;
    char destIPAddr[IP_SIZE];
    NpInt32 destPort;
    char srcIPAddr[IP_SIZE];
    NpInt32 srcPort;
    NpUint32 numOfHops;
    NpUint32 payloadSize;
    Packet payload;
} OdrAppMsgPacket;

/** Determine the type of received ODR packet
    @param [in] data The field indicates the type of the packet.
    @return the type of packet (RREQ, RREP, or AppMsg)
*/
const ODRPacketType DetermineODRPacketType( const NpInt32 data );

/** Initialzie an RREQ packet
    @param [out] packet The RREQ packet that needs initializing
*/
void InitOdrRreqPacket( OdrRreqPacket *packet );

void BuildOdrRreqPacket
    (
    OdrRreqPacket *packet,
    char *srcIPAddr,
    const NpUint32 broadcastId,
    char *destIPAddr,
    const NpUint32 numOfHops,
    const NpBool hasBeenSent,
    const NpBool forceRediscovery
    );

/** Initialzie an RREP packet
    @param [out] packet The RREP packet that needs initializing
*/
void InitOdrRrepPacket( OdrRrepPacket *packet );

void BuildOdrRrepPacket
    (
    OdrRrepPacket *packet,
    char *srcIPAddr,
    char *destIPAddr,
    const NpUint32 numOfHops,
    const NpBool forceRediscovery
    );

void DebugOdrRreqPacket( OdrRreqPacket *packet );

void DebugOdrRrepPacket( OdrRrepPacket *packet );

/** Make a copy of RREP packet
    @param [out] destPacket The copied version of RREP packet.
    @param [in] srcPacket The RREP packet that needs copying
*/
void CopyRrepPacket( OdrRrepPacket *destPacket, OdrRrepPacket *srcPacket );

void InitRREQQueue( RREQQueue *queue );

QueryRREQQueueResult FindEntryInRREQQueue
    (
    NpInt32 *foundIndex,
    RREQQueue *queue,
    const NpUint32 queueCount,
    const NpUint32 broadcastId,
    char *srcIPAddr
    );

void AddEntryToRREQQueue
    (
    RREQQueue *queue,
    NpInt32 *lastIndexOfRREQQueue,
    const NpUint32 broadcastId,
    char *srcIPAddr
    );

void UpdateEntryInRREQQueue
    (
    RREQQueue *queue,
    const NpInt32 index,
    const NpUint32 broadcastId
    );

void InitOdrAppMsgPacket
    (
    OdrAppMsgPacket *odrAppMsgPacket
    );

/** Build ODR application messgae packet
    @param [out] odrAppMsgPacket The packet that needs composing
    @param [in] packet The packet used to build the ODR application message
*/
void BuildOdrAppMsgPacket
    (
    OdrAppMsgPacket *odrAppMsgPacket,
    char *destIPAddr,
    const NpInt32 destPort,
    char *srcIPAddr,
    const NpInt32 srcPort,
    const NpUint32 numOfHops,
    const NpUint32 payloadSize,
    Packet *packet
    );

void CopyOdrAppMsgPacket
    (
    OdrAppMsgPacket *destPacket,
    OdrAppMsgPacket *srcPacket
    );

void AddEntryToAppMsgQueue
    (
    OdrAppMsgPacket *queue,
    NpInt32 *lastIndexOfQueue,
    OdrAppMsgPacket *appMsgPacketToBeAdded
    );

void RemoveAppMsgPacketAndShrinkAppMsgQueue
    (
    OdrAppMsgPacket *queue,
    NpInt32 *lastIndexOfQueue,
    const NpUint32 indexRemoved
    );

void DebugOdrAppMsgPacket( OdrAppMsgPacket *packet );

#endif

