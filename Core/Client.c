/**
   This implements the client's main functionality.
   @file    Client.c
   @author  Wei-Ying Tsai
   @contact wetsai@cs.stonybrook.edu
   @date 2015.11.22
*/

#include "unp.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "CommonTasks.h"
#include "MsgHandler.h"
#include "CommonType.h"
#include "Config.h"
#include "NpType.h"

#ifdef DEBUG_FORCE_REDISCOVERY
#undef DEBUG_FORCE_REDISCOVERY
#endif

// Let the user select whether or not force rediscovery when
// the client sends the time request. Uncomment this macro
// if you want to know if force rediscovery works well
//#define DEBUG_FORCE_REDISCOVERY

NpInt32 main( NpInt32 argc, char *argv[] )
{
    NpInt32 sockFd;
    struct sockaddr_un cliaddr;
    struct sockaddr_un servaddr;
    bzero( &cliaddr, sizeof( cliaddr ) );
    bzero( &servaddr, sizeof( servaddr ) );

    sockFd = Socket( AF_LOCAL, SOCK_DGRAM, 0 );

    // http://linux.die.net/man/3/mkstemp
    // The last six characters of template must be "XXXXXX" and these
    // are replaced with a string that makes the filename unique.
    // Since it will be modified, template must not be a string
    // constant, but should be declared as a character array.
    char template[MAX_LINE];
    bzero( &template, MAX_LINE );
    bcopy( TEMPLATE_PATH, template, strlen( TEMPLATE_PATH ) );
    NpInt32 fd = mkstemp( template );
    if( -1 == fd )
    {
        err_sys( "Error: creating temporary file fails..." );
    }
    close( fd );
    unlink( template );

    cliaddr.sun_family = AF_LOCAL;
    bcopy( template, cliaddr.sun_path, strlen(template) );

    Bind( sockFd, (SA *) &cliaddr, sizeof( cliaddr ) );

    char thisHostNode[MAX_LINE];
    GetHostName( thisHostNode, MAX_LINE );

    for( ; ; )
    {
        NpBool alreadyRetransmitted = FALSE;
        NpInt32 serverNode = 0;
        char hostName[MAX_LINE];
        bzero( &hostName, MAX_LINE );
        struct hostent *pRemoteHost;

        printf("Please input the server node (1~10, number only) or 0 to exit: vm");
        if( scanf( "%d", &serverNode ) <= 0 )
        {
            break;
        }

        if( 0 == serverNode )
        {
            printf( "I will miss you. Bye.\n" );
            break;
        }

        // https://msdn.microsoft.com/en-us/library/windows/desktop/ms738524%28v=vs.85%29.aspx
        // Get remost host (server)
        sprintf( hostName, "vm%d", serverNode );
        pRemoteHost = gethostbyname( hostName );
        if( NULL == pRemoteHost )
        {
            err_sys( "Error: gethostbyname fails..." );
        }

        // Get server IP address
        char IPAddr[IP_SIZE];
        bzero( &IPAddr, IP_SIZE );
        Inet_ntop(AF_INET, *(pRemoteHost->h_addr_list), IPAddr, sizeof(IPAddr) );

        NpInt32 forceRediscover = 0;
    #ifdef DEBUG_FORCE_REDISCOVERY
        printf("Force route rediscovery (0, 1)?");
        if( scanf( "%d", &forceRediscover ) <= 0 )
        {
            break;
        }
    #endif
        char buffer[MSG_SIZE];
        bzero( &buffer, MSG_SIZE );
        char toHostNode[MAX_LINE];
        bzero( &toHostNode, MAX_LINE );
        bcopy( TIME_MESSAGE, buffer, strlen( TIME_MESSAGE ) );
        msg_send( sockFd, IPAddr, SERVER_PORT, buffer, forceRediscover );
        IPAddrToHostName( toHostNode, IPAddr );
        PrintClientSendRequestMsg( thisHostNode, toHostNode );

        // Wait for response. Must process timeout once, as dictated
        // in the assignment specification.
        fd_set rset;
        fd_set allrset;
        FD_ZERO( &rset );
        FD_ZERO( &allrset );
        FD_SET( sockFd, &allrset );
        NpInt32 maxfd = sockFd;

        for( ; ; )
        {
            rset = allrset;

            struct timeval timeOut;
            // Must set both fields (tv_sec and tv_usec). Leaving one of
            // the field uninitialized results in undesired behavior.
            timeOut.tv_sec = 5;
            timeOut.tv_usec = 0;
            NpInt32 nReady = Select( (maxfd+1), &rset, NULL, NULL, &timeOut );
            if( FD_ISSET( sockFd, &rset ) )
            {
                char buffer[MAX_LINE];
                char remoteIPAddr[MAX_LINE];
                NpInt32 remotePort;
                bzero( &buffer, MAX_LINE );
                bzero( &remoteIPAddr, MAX_LINE );

                msg_recv( sockFd, buffer, remoteIPAddr, &remotePort);

                if( 0 != bcmp( buffer, TIME_MESSAGE, strlen( TIME_MESSAGE ) ) )
                {
                    char fromHostNode[MAX_LINE];
                    bzero( &fromHostNode, MAX_LINE );
                    IPAddrToHostName( fromHostNode, remoteIPAddr );
                    PrintClientReceiveResponseMsg
                        (
                        thisHostNode,
                        fromHostNode,
                        buffer
                        );
                    break;
                }
            }

            // Time out. Let me send the message one more time
            if( 0 == nReady )
            {
                if( alreadyRetransmitted )
                {
                    printf( "Error: timeout twice. Perhaps the server is down. Try another one.\n" );
                    break;
                }

                PrintClientTimeoutMsg( thisHostNode, toHostNode );
                printf( "Set force rediscovery to 1\n" );
                forceRediscover = 1;
                alreadyRetransmitted = TRUE;
                msg_send( sockFd, IPAddr, SERVER_PORT, buffer, forceRediscover );
                PrintClientSendRequestMsg( thisHostNode, toHostNode );
            }
        }
    }

    return 0;
}
