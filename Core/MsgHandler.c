/**
    This implements function outputing communication history
    @file    MsgHandler.c
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.24
*/

#include "MsgHandler.h"
#include <stdio.h>
#include "OdrPacket.h"
#include "NetworkInterface.h"
#include "Config.h"
#include "NpType.h"

//=============================================================================
void PrintClientSendRequestMsg( char *fromNode, char *toNode )
{
    printf( "client at node %s sending request to server at %s\n", fromNode, toNode );
}

//=============================================================================
void PrintClientReceiveResponseMsg
    (
    char *atNode,
    char *fromNode,
    char *message
    )
{
    printf( "client at node %s: received from %s <%s>\n", atNode, fromNode, message );
}

//=============================================================================
void PrintClientTimeoutMsg( char *atNode, char *fromNode )
{
    printf( "client at node %s: timeout on response from %s\n", atNode, fromNode );
}

//=============================================================================
void PrintServerSendResponseMsg( char *atNode, char *fromNode )
{
    printf( "server at node %s responding to request from %s\n", atNode, fromNode );
}

//=============================================================================
void PrintOdrSendRreqMsg
    (
    char *atNode,
    char *fromNode,
    char *toNode,
    char *toMACAddr
    )
{
    char macPresentationAddr[MAC_PRESENTATION_SIZE];
    bzero( &macPresentationAddr, MAC_PRESENTATION_SIZE );

    char *ptr = toMACAddr;
    NpUint32 i = MAC_SIZE;
    do {
        char segment[4];
        bzero( &segment, 4 );

        sprintf( segment,"%.2x%s", *ptr++ & 0xff, (i == 1) ? "" : ":");
        const catLength = ( i == 1 )? 2 : 3;
        strncat( macPresentationAddr, segment, catLength );
    } while (--i > 0);

    printf( "ODR at node %s: sending - frame hdr - src: %s dest: %s\n", atNode, atNode, macPresentationAddr );

    printf( "ODR msg payload - message: %s type: RREQ src: %s dest: %s\n", /*Empty payload*/"", fromNode, toNode );
}

//=============================================================================
void PrintOdrSendRrepMsg
    (
    char *atNode,
    char *fromNode,
    char *toNode,
    char *toMACAddr
    )
{
    char macPresentationAddr[MAC_PRESENTATION_SIZE];
    bzero( &macPresentationAddr, MAC_PRESENTATION_SIZE );

    char *ptr = toMACAddr;
    NpUint32 i = MAC_SIZE;
    do {
        char segment[4];
        bzero( &segment, 4 );

        sprintf( segment,"%.2x%s", *ptr++ & 0xff, (i == 1) ? "" : ":");
        const catLength = ( i == 1 )? 2 : 3;
        strncat( macPresentationAddr, segment, catLength );
    } while (--i > 0);

    printf( "ODR at node %s: sending - frame hdr - src: %s dest: %s\n", atNode, atNode, macPresentationAddr );

    printf( "ODR msg payload - message: %s type: RREP src: %s dest: %s\n", /*Empty payload*/"", fromNode, toNode );
}

//=============================================================================
void PrintOdrSendAppMsgMsg
    (
    char *atNode,
    char *fromNode,
    char *toNode,
    char *toMACAddr,
    char *payload
    )
{
    char macPresentationAddr[MAC_PRESENTATION_SIZE];
    bzero( &macPresentationAddr, MAC_PRESENTATION_SIZE );

    char *ptr = toMACAddr;
    NpUint32 i = MAC_SIZE;
    do {
        char segment[4];
        bzero( &segment, 4 );

        sprintf( segment,"%.2x%s", *ptr++ & 0xff, (i == 1) ? "" : ":");
        const catLength = ( i == 1 )? 2 : 3;
        strncat( macPresentationAddr, segment, catLength );
    } while (--i > 0);

    printf( "ODR at node %s: sending - frame hdr - src: %s dest: %s\n", atNode, atNode, macPresentationAddr );

    printf( "ODR msg payload - message: %s type: AppMsg src: %s dest: %s\n", payload, fromNode, toNode );
}

