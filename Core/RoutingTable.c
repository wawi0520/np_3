/**
   This implements the functions related to the routing table
   @file    RoutingTable.c
   @author  Wei-Ying Tsai
   @contact wetsai@cs.stonybrook.edu
   @date 2015.11.22
*/

#include <math.h>
#include <string.h>
#include "unp.h"
#include "RoutingTable.h"
#include "CommonTasks.h"

//=============================================================================
void InitRoutingTable( RoutingTable *table )
{
    NpUint32 i = 0;
    for( ; i < MAX_ENTRY_PER_ROUTING_TABLE; ++i )
    {
        table[i].isValid = FALSE;
        bzero( &table[i].destIP, IP_SIZE );
        table[i].outgoingInterfaceIndex = INVALID_INTERFACE_INDEX;
        bzero( &table[i].srcMacAddr, MAC_PRESENTATION_SIZE );
        table[i].numOfHops = 0;
        table[i].timeStamp = 0;
    }
}

//=============================================================================
void DebugRoutingTable( RoutingTable *table, const NpUint32 count )
{
    NpUint32 i = 0;
    printf("===============Routing Table===============\n");
    for( ; i < count; ++i )
    {
        if( FALSE == table[i].isValid )
        {
            continue;
            //err_sys( "An invalid entry is found in routing table. Exit..." );
        }
        printf( "Entry %u: \n", i+1 );
        printf( "\tIP Addr: %s\n", table[i].destIP );
        printf( "\tInterface index: %d\n", table[i].outgoingInterfaceIndex );
        printf( "\tMac Addr: %s\n", table[i].srcMacAddr );
        printf( "\t# of hops: %u\n", table[i].numOfHops );
        printf( "\tTimeStamp: %d\n", table[i].timeStamp );
    }
    printf("===========================================\n");
}

//=============================================================================
const NpBool FindEntryInRoutingTable
    (
    NpUint32 *indexFound,
    NpInt32 *outgoingInterfaceIndex,
    char *srcMacAddr,
    RoutingTable *table,
    const NpUint32 count,
    char *destIP
    )
{
    NpUint32 i = 0;
    for( ; i < count; ++i )
    {
        if( TRUE == table[i].isValid
            && 0 == bcmp( table[i].destIP, destIP, strlen( destIP ) ) )
        {
            *outgoingInterfaceIndex = table[i].outgoingInterfaceIndex;
            bcopy( table[i].srcMacAddr, srcMacAddr, strlen( table[i].srcMacAddr ) );
            *indexFound = i;
            return TRUE;
        }
    }

    return FALSE;
}

//=============================================================================
void SetRoutingEntriesInvalidIfNeeded
    (
    RoutingTable *table,
    const NpUint32 count,
    char *destIP,
    const NpInt32 staleness
    )
{
    NpUint32 i = 0;
    for( ; i < count; ++i )
    {
        if( 0 == bcmp( table[i].destIP, destIP, strlen( table[i].destIP ) )
            && staleness < (NpInt32)(abs( GetTimeStamp() - table[i].timeStamp) ) )
        {
            table[i].isValid = FALSE;
        }
    }
}

//=============================================================================
void SetRoutingEntriesInvalid
    (
    RoutingTable *table,
    const NpUint32 count,
    char *destIP
    )
{
    NpUint32 i = 0;
    for( ; i < count; ++i )
    {
        if( 0 == bcmp( table[i].destIP, destIP, strlen( destIP ) ) )
        {
            table[i].isValid = FALSE;
            break;
        }
    }
}

//=============================================================================
const NpBool AddOrUpdateEntryToRoutingTable
    (
    RoutingTable *routingTable,
    NpInt32 *lastIndexOfRoutingTable,
    char *IPAddr,
    const NpUint32 numOfHops,
    const NpInt32 interfaceIndex,
    char *MACPresentationAddr
    )
{
    NpBool outgoingInterfaceIndexChanged = FALSE;
    NpUint32 count = (NpUint32)(*lastIndexOfRoutingTable + 1);
    NpBool isUpdated = FALSE;
    NpUint32 i = 0;
    for( ; i < count; ++i )
    {
        if( 0 == bcmp( routingTable[i].destIP, IPAddr, strlen( IPAddr ) ) )
        {
            bzero( &routingTable[i].srcMacAddr, MAC_PRESENTATION_SIZE );

            routingTable[i].isValid = TRUE;
            // destIP is the same. Needn't update
            if( routingTable[i].outgoingInterfaceIndex != interfaceIndex )
            {
                routingTable[i].outgoingInterfaceIndex = interfaceIndex;
                outgoingInterfaceIndexChanged = TRUE;
            }
            bcopy( MACPresentationAddr, routingTable[i].srcMacAddr, strlen( MACPresentationAddr )  );
            routingTable[i].numOfHops = numOfHops;
            routingTable[i].timeStamp = GetTimeStamp();

            isUpdated = TRUE;
            break;
        }
    }

    if( FALSE == isUpdated )
    {
        // We need to add a new entry into routing table
        if( MAX_ENTRY_PER_ROUTING_TABLE == count )
        {
            err_sys( "Unable to add entry to routing table. The table is full" );
        }

        //printf( "IP Address: %s (%d)\n", IPAddr, (NpUint32)strlen(IPAddr) );
        //printf( "MAC Address: %s (%d)\n", MACPresentationAddr, (NpUint32)strlen(MACPresentationAddr) );
        bzero( &routingTable[i].destIP, IP_SIZE );
        bzero( &routingTable[i].srcMacAddr, MAC_PRESENTATION_SIZE );

        routingTable[i].isValid = TRUE;
        bcopy( IPAddr, routingTable[i].destIP, strlen( IPAddr ) );
        routingTable[i].outgoingInterfaceIndex = interfaceIndex;
        bcopy( MACPresentationAddr, routingTable[i].srcMacAddr, strlen( MACPresentationAddr )  );
        routingTable[i].numOfHops = numOfHops;
        routingTable[i].timeStamp = GetTimeStamp();

        *lastIndexOfRoutingTable = *lastIndexOfRoutingTable + 1;
        ++count;
        // Because we add a new entry, the outgoing interface
        // index are also newly created. So it is viewed as changed.
        outgoingInterfaceIndexChanged = TRUE;
    }

    if( TRUE == isUpdated )
    {
        printf( "Update an entry (dest ip:%s) into routing table.\n", IPAddr );
    }
    else
    {
        printf( "Add a new entry into routing table.\n" );
    }
    //DebugRoutingTable( routingTable, count );

    return outgoingInterfaceIndexChanged;
}

//=============================================================================
void UpdateEntryWithTimeStampInRoutingTable
    (
    RoutingTable *routingTable,
    const NpUint32 count,
    char *destIPAddr
    )
{
    NpUint32 i = 0;
    NpBool timeStampUpdated = FALSE;
    for( ; i < count; ++i )
    {
        if(  0 == bcmp( destIPAddr, routingTable[i].destIP, strlen( destIPAddr ) ) )
        {
            routingTable[i].isValid = TRUE;
            routingTable[i].timeStamp = GetTimeStamp();
            timeStampUpdated = TRUE;
        }
    }

    if( FALSE == timeStampUpdated )
    {
        err_sys( "No entry is found when updating the time stamp. Exit..." );
    }
}

//=============================================================================
const NpUint32 GetHopCountFromRoutingTable
    (
    RoutingTable *routingTable,
    const NpUint32 count,
    char *destIPAddr
    )
{
    NpUint32 numOfHops = 0;
    NpBool hopCountExist = FALSE;
    NpUint32 i = 0;
    for( ; i < count; ++i )
    {
        if( 0 == bcmp( destIPAddr, routingTable[i].destIP, strlen( destIPAddr ) ) )
        {
            routingTable[i].isValid  = TRUE;
            numOfHops = routingTable[i].numOfHops;
            hopCountExist = TRUE;
        }
    }

    if( FALSE == hopCountExist )
    {
        err_sys( "No entry is found when getting the hop count. Exit..." );
    }

    return numOfHops;
}

