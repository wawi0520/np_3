/**
    This implements data structure and function related to client/server
    @file    CommonTasks.c
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.24
*/

#include "CommonTasks.h"
#include <string.h>
#include "unp.h"

//=============================================================================
void DebugPacketData( Packet *packetData )
{
    printf( "===============Packet Data===============\n" );
    printf( "Message    : %s\n", packetData->message );
    printf( "IP Addrress: %s\n", packetData->IPAddr );
    printf( "port       : %d\n", packetData->port );
    printf( "flag       : %d\n", packetData->flag );
    printf( "=========================================\n" );
}

//=============================================================================
void InitPacketData( Packet *packetData )
{
    // Clear packet first
    bzero( packetData->message, MSG_SIZE );
    bzero( packetData->IPAddr, IP_SIZE );
    packetData->port = -1;
    packetData->flag = -1;
}

//=============================================================================
void BuildPacketData
    (
    Packet *packetData,
    char *message,
    char *IPAddr,
    const NpInt32 port,
    const NpInt32 flag
    )
{
    // Clear packet first
    bzero( packetData, sizeof( Packet ) );

    bcopy( message, packetData->message, strlen( message ) );
    bcopy( IPAddr, packetData->IPAddr, strlen( IPAddr ) );
    packetData->port = port;
    packetData->flag = flag;
}

//=============================================================================
void ParsePacketData
    (
    char *message,
    char *IPAddr,
    NpInt32 *port,
    NpInt32 *flag,
    Packet *packetData
    )
{
    bzero( message, MSG_SIZE );
    bzero( IPAddr, IP_SIZE );

    bcopy( packetData->message, message, strlen( packetData->message ) );
    bcopy( packetData->IPAddr, IPAddr, strlen( packetData->IPAddr ) );
    *port = packetData->port;
    *flag = packetData->flag;
}

//=============================================================================
void msg_send
    (
    const NpInt32 sockFd,
    char *IPAddr,
    const NpInt32 port,
    char *msg,
    const NpInt32 flag
    )
{
    Packet packetData;
    bzero( &packetData, sizeof( Packet ) );

    bcopy( msg, packetData.message, strlen( msg ) );
    bcopy( IPAddr, packetData.IPAddr, strlen( IPAddr ) );
    packetData.port = port;
    packetData.flag = flag;

    struct sockaddr_un toAddr;
    bzero( &toAddr, sizeof( toAddr ) );
    toAddr.sun_family = AF_LOCAL;
    bcopy( ODR_PATH, toAddr.sun_path, strlen( ODR_PATH ) );

    Sendto(
        sockFd,
        (char *)&packetData, // cast to string
        sizeof( Packet ),
        0,
        (SA *)&toAddr,
        sizeof( struct sockaddr_un )
        );
}

//=============================================================================
void msg_recv
    (
    const NpInt32 sockFd,
    char *msg,
    char *IPAddr,
    NpInt32 *port
    )
{
    bzero( msg, MSG_SIZE );
    bzero( IPAddr, IP_SIZE );

    Packet packetData;
    bzero( &packetData, sizeof( Packet ) );

    struct sockaddr_un fromAddr;
    bzero( &fromAddr, sizeof( fromAddr ) );
    fromAddr.sun_family = AF_LOCAL;
    bcopy( ODR_PATH, fromAddr.sun_path, strlen( ODR_PATH ) );
    NpUint32 len = sizeof( fromAddr );

    const NpUint32 nRead = Recvfrom(
                                sockFd,
                                (char *) &packetData,
                                sizeof( Packet ),
                                0,
                                (SA*) &fromAddr,
                                &len);

    bcopy( packetData.IPAddr, IPAddr, strlen( packetData.IPAddr ) );
    bcopy( packetData.message, msg, strlen( packetData.message ) );
    *port = packetData.port;
}

//=============================================================================
void IPAddrToHostName( char *hostname, char *IPAddr )
{
    bzero( hostname, MAX_LINE );

    struct hostent *host;
    struct in_addr addr;
    bzero( &addr, sizeof( addr ) );

    Inet_pton( AF_INET, IPAddr, &addr );
    host = gethostbyaddr((char *) &addr, 4, AF_INET);

    bcopy( host->h_name, hostname, strlen( host->h_name ) ) ;
}

//=============================================================================
NpInt32 GetTimeStamp()
{
    return (NpInt32) (difftime( time(NULL), 0 ));
}

//=============================================================================
void GetHostName( char* hostName, const NpUint32 len )
{
    bzero( hostName, len );
    if( -1 == gethostname( hostName, len ) )
    {
        err_sys("Error: gethostname fails");
    }
}

//=============================================================================
NpInt32
MySelect
    (
    NpInt32 nfds,
    fd_set *readfds,
    fd_set *writefds,
    fd_set *exceptfds,
    struct timeval *timeout
    )
{
    NpInt32 n;
    if ( (n = select(nfds, readfds, writefds, exceptfds, timeout)) < 0)
    {
        if( EINTR == errno )
        {
            //err_msg( "Non-fatal select error\n" );
        }
        else
        {
            err_quit( "select error" );
        }
    }
    return(n);      /* can return 0 on timeout */
}


