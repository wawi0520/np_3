/**
    This declares the struct and functions related to the
    network interface.
    @file    NetworkInterface.h
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.22
*/

#ifndef _NETWORK_INTERFACE_H_
#define _NETWORK_INTERFACE_H_
#include "Config.h"
#include "NpType.h"


typedef struct
{
    NpInt32 interfaceIndex;
    char name[IF_NAME_SIZE];
    char IPAddr[IP_SIZE];
    char hwAddr[MAC_PRESENTATION_SIZE];
} NeworkInterfacesTable;

/** Initialize the network interface table
    @param [in/out] table The network interface table
    @return the number of network interfaces
*/
const NpUint32 InitNetworkInterfaceTable( NeworkInterfacesTable *table );

/** Print the network interface table
    @param [in] table The network interface table
    @param [in] count The number of network interfaces
*/
void DebugNetworkInterfaceTable( NeworkInterfacesTable *table, const NpUint32 count );


// Helper function
// Presentation format -> xx:xx:xx:xx:xx:xx
// Succinct format -> xxxxxxxxxxxxxx
void ToMACPresentationFormat( char *presentation, char *succinct );
void ToMACSuccinctFormat( char *succinct, char *presentation );


#endif

