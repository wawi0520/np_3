/**
   This declares the struct and functions related to the
   routing table.
   @file    RoutingTable.h
   @author  Wei-Ying Tsai
   @contact wetsai@cs.stonybrook.edu
   @date 2015.11.22
*/

#ifndef _ROUTING_TABLE_H_
#define _ROUTING_TABLE_H_
#include "CommonType.h"
#include "Config.h"
#include "NpType.h"

/** The routing table
*/
typedef struct
{
    NpBool isValid;                         //!< Specify the status of the entry
    char destIP[IP_SIZE];                   //!< The destination IP address
    NpInt32 outgoingInterfaceIndex;         //!< The index associated with the outgoing network interface
    char srcMacAddr[MAC_PRESENTATION_SIZE]; //!< The outgoing MAC address
    NpUint32 numOfHops;
    NpInt32 timeStamp;                      //!< The time that the entry is recently added, updated, or re-confirmed.
} RoutingTable;


/** Initialize the routing table
    @param [in/out] table The routing table
*/
void InitRoutingTable( RoutingTable *table );

/** Find in the routing table an entry, given the destination
    IP address
    @param [out] indexFound The index to the entry that
                    matches the given  destIP
    @param [out] outgoingInterfaceIndex The index to the
                    outgoing interface
    @param [out] srcMacAddr The source MAC address
    @param [in] table The routing table
    @param [in] destIP The destination IP address
    @return TRUE if an entry is found; FALSE otherwise
*/
const NpBool FindEntryInRoutingTable
    (
    NpUint32 *indexFound,
    NpInt32 *outgoingInterfaceIndex,
    char *srcMacAddr,
    RoutingTable *table,
    const NpUint32 count,
    char *destIP
    );

/** Invalidate the routing entry if the force rediscovery is set
    or the entry is expired.
    @param [in/out] table The routing table
    @param [in] count The number of entries in the routing table
    @param [in] destIP Destination IP address for looking up
    @param [in] staleness The time out value in second
*/
void SetRoutingEntriesInvalidIfNeeded
    (
    RoutingTable *table,
    const NpUint32 count,
    char *destIP,
    const NpInt32 staleness
    );

/** Set an routing entry invalid according to the given address
    @param [in/out] table The routing table
    @param [in] count The number of entries in the routing table
    @param [in] destIP The IP address for looking up
*/
void SetRoutingEntriesInvalid
    (
    RoutingTable *table,
    const NpUint32 count,
    char *destIP
    );

/** Add or update an routing entry
    First try to find the destination IP address. If such entry
    associated with that address exists, we update it. Otherwise,
    we add a new routing entry into routing table.
    @param [in/out] routingTable The routing table
    @param [in/out] lastIndexOfRoutingTable The index associated
                    with the last valid routing entry.
    @param [in] IPAddr The destination IP address for look up.
    @param [in] numOfHops The number of hops.
    @param [in] interfaceIndex The index associated with the
                    outgoing network interface
    @param [in] MACPresentationAddr The MAC address in presentation
                    format.
    @return TRUE if the outgoing network inteface index is changed
                    during adding or updating the entry; FALSE otherwise.
    @note The precise meaning of the returned value is regarding whether
        the outgoing interface index is added (if an new routing entry is
        added) or populated (if an routing entry is updated). In this case,
        we must retransmit something (RREQ/RREP).
*/
const NpBool AddOrUpdateEntryToRoutingTable
    (
    RoutingTable *routingTable,
    NpInt32 *lastIndexOfRoutingTable,
    char *IPAddr,
    const NpUint32 numOfHops,
    const NpInt32 interfaceIndex,
    char *MACPresentationAddr
    );

/** Update the time stamp of a given routing entry
    @param [in/out] routingTable The routing table
    @param [in] The number of entries in the routing table
    @param [in] destIPAddr The destination IP address for lookup up
*/
void UpdateEntryWithTimeStampInRoutingTable
    (
    RoutingTable *routingTable,
    const NpUint32 count,
    char *destIPAddr
    );

/** Update the time stamp of a given routing entry
    @param [in] routingTable The routing table
    @param [in] The number of entries in the routing table
    @param [in] destIPAddr The destination IP address for lookup up
    @return the number of hops of an entry which matches the given
                destination IP address
*/
const NpUint32 GetHopCountFromRoutingTable
    (
    RoutingTable *routingTable,
    const NpUint32 count,
    char *destIPAddr
    );

#endif

