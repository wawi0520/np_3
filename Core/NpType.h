/**
    Network Programming Type
    This defines serveral types.
    @file    NpType.h
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.22
*/

#ifndef _NP_TYPE_H_
#define _NP_TYPE_H_

#include <time.h>

// Basic types
typedef enum {FALSE = 0, TRUE} NpBool;
typedef int NpInt32;
typedef unsigned int NpUint32;
typedef double NpDouble;
typedef unsigned char NpUchar;

#endif
