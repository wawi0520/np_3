/**
    This implements functions related to the ODR packets.
    @file    OdrPacket.c
    @author  Wei-Ying Tsai
    @contact wetsai@cs.stonybrook.edu
    @date 2015.11.22
*/

#include "OdrPacket.h"

//=============================================================================
const ODRPacketType DetermineODRPacketType( const NpInt32 data )
{
    if( 0 == data )
    {
        printf( "RREQ\n" );
        return ODR_RREQ;
    }
    else if( 1 == data )
    {
        printf( "RREP\n" );
        return ODR_RREP;
    }
    else if( 2 == data )
    {
        printf( "AppMsg\n" );
        return ODR_APPMSG;
    }
}

//=============================================================================
void InitOdrRreqPacket( OdrRreqPacket *packet )
{
    packet->odrType = ODR_RREQ;
    bzero( &packet->srcIPAddr, IP_SIZE );
    packet->broadcastId = 0;
    bzero( &packet->destIPAddr, IP_SIZE );
    packet->numOfHops = 0;
    packet->hasBeenSent = FALSE;
    packet->forceRediscovery = FALSE;
}

//=============================================================================
void BuildOdrRreqPacket
    (
    OdrRreqPacket *packet,
    char *srcIPAddr,
    const NpUint32 broadcastId,
    char *destIPAddr,
    const NpUint32 numOfHops,
    const NpBool hasBeenSent,
    const NpBool forceRediscovery
    )
{
    packet->odrType = ODR_RREQ;
    bcopy( srcIPAddr, packet->srcIPAddr, strlen( srcIPAddr ) );
    packet->broadcastId = broadcastId;
    bcopy( destIPAddr, packet->destIPAddr, strlen( destIPAddr ) );
    packet->numOfHops = numOfHops;
    packet->hasBeenSent = hasBeenSent;
    packet->forceRediscovery = forceRediscovery;
}

//=============================================================================
void InitOdrRrepPacket( OdrRrepPacket *packet )
{
    packet->odrType = ODR_RREP;
    bzero( &packet->srcIPAddr, IP_SIZE );
    bzero( &packet->destIPAddr, IP_SIZE );
    packet->numOfHops = 0;
    packet->forceRediscovery = FALSE;
}

//=============================================================================
void BuildOdrRrepPacket
    (
    OdrRrepPacket *packet,
    char *srcIPAddr,
    char *destIPAddr,
    const NpUint32 numOfHops,
    const NpBool forceRediscovery
    )
{
    packet->odrType = ODR_RREP;
    bcopy( srcIPAddr, packet->srcIPAddr, strlen( srcIPAddr ) );
    bcopy( destIPAddr, packet->destIPAddr, strlen( destIPAddr ) );
    packet->numOfHops = numOfHops;
    packet->forceRediscovery = forceRediscovery;
}

//=============================================================================
void DebugOdrRreqPacket( OdrRreqPacket *packet )
{
    printf("=============ODR RREQ Packet=============\n");
    printf( "Type (RREQ)\n" );
    printf( "srcIPAddr (%s)\n", packet->srcIPAddr );
    printf( "broadcastId (%u)\n", packet->broadcastId );
    printf( "destIPAddr (%s)\n", packet->destIPAddr );
    printf( "numOfHops (%u)\n", packet->numOfHops );
    printf( "hasBeenSent (%u)\n", (TRUE == packet->hasBeenSent)? 1: 0 );
    printf( "forceRediscovery (%u)\n", (TRUE == packet->forceRediscovery)? 1: 0 );
    printf("=========================================\n");
}

//=============================================================================
void DebugOdrRrepPacket( OdrRrepPacket *packet )
{
    printf("=============ODR RREP Packet=============\n");
    printf( "Type (RREP)\n" );
    printf( "srcIPAddr (%s)\n", packet->srcIPAddr );
    printf( "destIPAddr (%s)\n", packet->destIPAddr );
    printf( "numOfHops (%u)\n", packet->numOfHops );
    printf( "forceRediscovery (%u)\n", (TRUE == packet->forceRediscovery)? 1: 0 );
    printf("=========================================\n");
}

//=============================================================================
void CopyRrepPacket( OdrRrepPacket *destPacket, OdrRrepPacket *srcPacket )
{
    destPacket->odrType = srcPacket->odrType;
    bcopy( &srcPacket->srcIPAddr, &destPacket->srcIPAddr, IP_SIZE );
    bcopy( &srcPacket->destIPAddr, &destPacket->destIPAddr, IP_SIZE );
    destPacket->numOfHops = srcPacket->numOfHops;
    destPacket->forceRediscovery = srcPacket->forceRediscovery;
}

//=============================================================================
void InitRREQQueue( RREQQueue *queue )
{
    NpUint32 i = 0;
    for( ; i < MAX_RREQ_PER_ODR; ++i )
    {
        queue[i].valid = FALSE;
        queue[i].broadcastId = -1;
        bzero( queue[i].srcIPAddr, IP_SIZE );
    }
}

//=============================================================================
QueryRREQQueueResult
FindEntryInRREQQueue
    (
    NpInt32 *foundIndex,
    RREQQueue *queue,
    const NpUint32 queueCount,
    const NpUint32 broadcastId,
    char *srcIPAddr
    )
{
    QueryRREQQueueResult result = RESULT_RREQ_NOT_FOUND;
    *foundIndex = -1; // If no entry is found, this invalid index -1 will be returned.

    NpUint32 i = 0;
    for( ; i < queueCount; ++i )
    {
        if( 0 == bcmp( queue[i].srcIPAddr, srcIPAddr, strlen( srcIPAddr ) ) )
        {
            if( broadcastId == queue[i].broadcastId )
            {
                result = RESULT_RREQ_SAME_BID;
            }
            else if( broadcastId < queue[i].broadcastId )
            {
                result = RESULT_RREQ_SMALLER_BID;
            }
            else
            {
                result = RESULT_RREQ_LARGER_BID;
            }
            *foundIndex = (NpInt32)i;
            break;
        }
    }

    return result;
}

//=============================================================================
void AddEntryToRREQQueue
    (
    RREQQueue *queue,
    NpInt32 *lastIndexOfRREQQueue,
    const NpUint32 broadcastId,
    char *srcIPAddr
    )
{
    NpInt32 index = *lastIndexOfRREQQueue + 1;
    if( MAX_RREQ_PER_ODR == index )
    {
        err_sys( "Unable to add entry to RREQ queue. The queue is full" );
    }

    if( TRUE == queue[index].valid )
    {
        err_sys( "This should not happen..." );
    }

    queue[index].valid = TRUE;
    queue[index].broadcastId = broadcastId;
    bzero( &queue[index].srcIPAddr, IP_SIZE );
    bcopy( srcIPAddr, queue[index].srcIPAddr, strlen( srcIPAddr ) );
    *lastIndexOfRREQQueue = *lastIndexOfRREQQueue + 1;
}

//=============================================================================
void UpdateEntryInRREQQueue
    (
    RREQQueue *queue,
    const NpInt32 index,
    const NpUint32 broadcastId
    )
{
    if( index < 0 )
    {
        err_sys( "Try to access an illegal entry in RREQ queue. Exit..." );
    }
    queue[index].broadcastId = broadcastId;
}

//=============================================================================
void InitOdrAppMsgPacket
    (
    OdrAppMsgPacket *odrAppMsgPacket
    )
{
    bzero( &odrAppMsgPacket->destIPAddr, IP_SIZE );
    odrAppMsgPacket->destPort = -1;
    bzero( &odrAppMsgPacket->srcIPAddr, IP_SIZE );
    odrAppMsgPacket->srcPort = -1;
    odrAppMsgPacket->numOfHops = 0;
    odrAppMsgPacket->payloadSize = 0;
    InitPacketData( &odrAppMsgPacket->payload );
}

//=============================================================================
void BuildOdrAppMsgPacket
    (
    OdrAppMsgPacket *odrAppMsgPacket,
    char *destIPAddr,
    const NpInt32 destPort,
    char *srcIPAddr,
    const NpInt32 srcPort,
    const NpUint32 numOfHops,
    const NpUint32 payloadSize,
    Packet *packet
    )
{
    bzero( &odrAppMsgPacket->destIPAddr, IP_SIZE );
    bzero( &odrAppMsgPacket->srcIPAddr, IP_SIZE );
    bzero( &odrAppMsgPacket->payload, MSG_SIZE );

    odrAppMsgPacket->odrType = ODR_APPMSG;
    bcopy( destIPAddr, odrAppMsgPacket->destIPAddr, strlen( destIPAddr ) );
    odrAppMsgPacket->destPort = destPort;
    bcopy( srcIPAddr, odrAppMsgPacket->srcIPAddr, strlen( srcIPAddr ) );
    odrAppMsgPacket->srcPort = srcPort;
    odrAppMsgPacket->numOfHops = numOfHops;
    odrAppMsgPacket->payloadSize = payloadSize;
    BuildPacketData
        (
        &odrAppMsgPacket->payload,
        packet->message,
        packet->IPAddr,
        packet->port,
        packet->flag
        );
}

//=============================================================================
void CopyOdrAppMsgPacket
    (
    OdrAppMsgPacket *destPacket,
    OdrAppMsgPacket *srcPacket
    )
{
    bzero( &destPacket->destIPAddr, IP_SIZE );
    bzero( &destPacket->srcIPAddr, IP_SIZE );
    bzero( &destPacket->payload, MSG_SIZE );

    destPacket->odrType = ODR_APPMSG;
    bcopy( srcPacket->destIPAddr, destPacket->destIPAddr, strlen( srcPacket->destIPAddr ) );
    destPacket->destPort = srcPacket->destPort;
    bcopy( srcPacket->srcIPAddr, destPacket->srcIPAddr, strlen( srcPacket->srcIPAddr ) );
    destPacket->srcPort = srcPacket->srcPort;
    destPacket->numOfHops = srcPacket->numOfHops;
    destPacket->payloadSize = srcPacket->payloadSize;
    BuildPacketData
        (
        &destPacket->payload,
        srcPacket->payload.message,
        srcPacket->payload.IPAddr,
        srcPacket->payload.port,
        srcPacket->payload.flag
        );
}

//=============================================================================
void AddEntryToAppMsgQueue
    (
    OdrAppMsgPacket *queue,
    NpInt32 *lastIndexOfQueue,
    OdrAppMsgPacket *appMsgPacketToBeAdded
    )
{
    if( MAX_APPMSG_PER_ODR == (*lastIndexOfQueue) + 1 )
    {
        err_sys( "Unable to add entry to AppMsg queue. The queue is full" );
    }

    CopyOdrAppMsgPacket( &queue[*lastIndexOfQueue+1], appMsgPacketToBeAdded );
    *lastIndexOfQueue = *lastIndexOfQueue + 1;
}

//=============================================================================
void RemoveAppMsgPacketAndShrinkAppMsgQueue
    (
    OdrAppMsgPacket *queue,
    NpInt32 *lastIndexOfQueue,
    const NpUint32 indexRemoved
    )
{
    if( indexRemoved > *lastIndexOfQueue )
    {
        err_sys( "The AppMsg packet that needs removing is not in the queue. Exit..." );
    }

    if( 0 == ( ( *lastIndexOfQueue ) + 1) )
    {
        err_sys( "Why do you want to remove an RREP from an empty queue...? Exit..." );
    }

    NpUint32 movedCandidate = *lastIndexOfQueue;
    if( movedCandidate > indexRemoved )
    {
        CopyOdrAppMsgPacket( &queue[indexRemoved], &queue[movedCandidate] );
    }

    *lastIndexOfQueue = *lastIndexOfQueue - 1;
}

//=============================================================================
void DebugOdrAppMsgPacket( OdrAppMsgPacket *packet )
{
    printf("============ODR AppMsg Packet============\n");
    printf( "Type (AppMsg)\n" );
    printf( "destIPAddr (%s)\n", packet->destIPAddr );
    printf( "destPort (%d)\n", packet->destPort );
    printf( "srcIPAddr (%s)\n", packet->srcIPAddr );
    printf( "srcPort (%d)\n", packet->srcPort );
    printf( "numOfHops (%u)\n", packet->numOfHops );
    printf( "payloadSize (%u)\n", packet->payloadSize );
    printf( "payload:\n" );
    printf( "\tMessage: %s\n", packet->payload.message);
    printf( "\tIP Address: %s\n", packet->payload.IPAddr );
    printf( "\tport: %d\n", packet->payload.port );
    printf( "\tflag: %d\n", packet->payload.flag );
    printf("=========================================\n");
}

