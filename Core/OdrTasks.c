/**
   This implements functions related to the ODR tasks.
   @file    OdrTasks.h
   @author  Wei-Ying Tsai
   @contact wetsai@cs.stonybrook.edu
   @date 2015.11.22
*/

#include "OdrTasks.h"
#include <string.h>
#include <sys/socket.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <linux/if_arp.h>
#include "unp.h"
#include "Config.h"
#include "NpType.h"

//#ifndef ETH_ALEN
//#define ETH_ALEN 6
//#endif

const NpInt32 PATH_PORT_TABLE_TTL_SEC = 600; // 10 minutes
//const NpUint32 ETH_FRAME_LEN = 1518;

//=============================================================================
void InitPathPortTable( PathPortTable *table )
{
    NpUint32 i = 0;
    for( i = 0; i < MAX_APP_PER_ODR; ++i )
    {
        table[i].isValid = FALSE;
        bzero( &table[i].sunPath, SUN_PATH_SIZE );
        table[i].port = 0;
        table[i].timeStamp = 0;
    }
}

//=============================================================================
NpBool AddEntry2PathPortTable
    (
    PathPortTable *table,
    char *path,
    NpInt32 port
    )
{
    if( NULL == table && NULL == path )
    {
        err_sys( "The entry to be added is corrupted. Exit..." );
    }

    NpUint32 i = 0;
    NpInt32 currentTimeStamp = GetTimeStamp();
    for( i = 0; i < MAX_APP_PER_ODR; ++i )
    {
        NpBool overwriteEntry = ( FALSE == table[i].isValid )
                                || ( 0 == table[i].timeStamp? TRUE : FALSE )
                                || currentTimeStamp < table[i].timeStamp
                                || ( currentTimeStamp - table[i].timeStamp ) > PATH_PORT_TABLE_TTL_SEC;
        if( TRUE == overwriteEntry )
        {
            // We find a space where we could insert the entry
            table[i].isValid = TRUE;
            bzero( &table[i].sunPath, SUN_PATH_SIZE );
            bcopy( path, table[i].sunPath, strlen( path ) );
            table[i].port = port;
            table[i].timeStamp = GetTimeStamp();
            return TRUE;
        }
    }

    err_sys( "Reach the maximum number of entries in PathPortTable. Exit..." );
    return FALSE;
}

//=============================================================================
NpBool GetPortFromPathPortTable
    (
    NpInt32 *port,
    PathPortTable *table,
    char *path
    )
{
    NpUint32 i = 0;
    for( i = 0; i < MAX_APP_PER_ODR; ++i )
    {
        if( TRUE == table[i].isValid
            && strlen( table[i].sunPath ) == strlen( path )
            && 0 == bcmp( table[i].sunPath, path, strlen(path) ) )
        {
            // This entry is found. Update the time stamp because
            // the entry is recently used.
            table[i].timeStamp = GetTimeStamp();
            *port = table[i].port;
            return TRUE;
        }
    }

    return FALSE;
}

NpBool GetPathFromPathPortTable
    (
    char *path,
    PathPortTable *table,
    const NpInt32 port
    )
{
    NpUint32 i = 0;
    for( i = 0; i < MAX_APP_PER_ODR; ++i )
    {
        if( TRUE == table[i].isValid
            && table[i].port == port )
        {
            // This entry is found. Update the time stamp because
            // the entry is recently used.
            bzero( &path[0], SUN_PATH_SIZE );
            bcopy( table[i].sunPath, path, strlen( table[i].sunPath ) );
            table[i].timeStamp = GetTimeStamp();
            return TRUE;
        }
    }

    return FALSE;
}

//=============================================================================
void DebugPathPortTable( PathPortTable *table )
{
    NpUint32 i = 0;
    printf("===============PathPortTable===============\n");
    for( i = 0; i < MAX_APP_PER_ODR; ++i )
    {
        const NpUint32 valid = (TRUE == table[i].isValid)? 1: 0;
        printf( "%u: %u %s %d %d\n", i+1, valid, table[i].sunPath, table[i].port, table[i].timeStamp );
    }
    printf("===========================================\n");
}

//=============================================================================
void FloodRREQ
    (
    const NpInt32 sockFd,
    char *srcIPAddr,
    const NpUint32 broadcastId,
    char *destIPAddr,
    const NpUint32 numOfHops,
    const NpBool hasBeenSent,
    const NpBool forceRediscovery,
    const NpInt32 ignoredInterfaceIndex,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    )
{
    // Adapted from: http://aschauf.landshut.org/fh/linux/udp_vs_raw/ch01s03.html#id2766267

    /*target address*/
    struct sockaddr_ll socket_address;
    /*buffer for ethernet frame*/
    void* buffer = (void*)calloc(ETH_FRAME_LEN,1);

    /*pointer to ethenet header*/
    unsigned char* etherhead = buffer;

    /*userdata in ethernet frame*/
    unsigned char* data = buffer + ETHER_FRAME_HEADER_SIZE;

    /*another pointer to ethernet header*/
    struct ethhdr *eh = (struct ethhdr *)etherhead;

    /*other host MAC address*/
    NpUchar dest_mac[MAC_SIZE];
    // The host MAC address is the broadcast address
    memset( dest_mac, 0xFF, MAC_SIZE );
    /*char pres[MAC_PRESENTATION_SIZE];
    bzero( &pres,MAC_PRESENTATION_SIZE );
    ToMACPresentationFormat( pres,dest_mac);*/

    /* prepare sockaddr_ll */
    // RAW communication
    socket_address.sll_family = PF_PACKET;
    // we don't use a protocoll above ethernet layer; just use anything here
    socket_address.sll_protocol = htons( ETH_P_IP );
    // ARP hardware identifier is ethernet
    socket_address.sll_hatype = ARPHRD_ETHER;
    // target is another host
    socket_address.sll_pkttype = PACKET_OTHERHOST;
    // address length
    socket_address.sll_halen = ETH_ALEN;

    NpUint32 i = 0;
    for( ; i < numOfInterfaces; ++i )
    {
        if( ignoredInterfaceIndex == ifTable[i].interfaceIndex
            || 0 == bcmp( ifTable[i].name, "eth0", strlen("eth0") )
            || 0 == bcmp( ifTable[i].name, "lo", strlen("lo") ) )
        {
            // Skip eth0 and loopback address
            continue;
        }

        ToMACSuccinctFormat( socket_address.sll_addr, ifTable[i].hwAddr );
        socket_address.sll_addr[6] = 0x00;
        socket_address.sll_addr[7] = 0x00;

        socket_address.sll_ifindex = ifTable[i].interfaceIndex;

        /*set the frame header*/
        memcpy((void*)buffer, (void*)dest_mac, ETH_ALEN);
        memcpy((void*)(buffer+ETH_ALEN), (void*)socket_address.sll_addr, ETH_ALEN);
        eh->h_proto = htons( MY_PROTOCOL );

        /*set up frame's payload*/
        OdrRreqPacket rreqPacket;
        InitOdrRreqPacket( &rreqPacket );
        BuildOdrRreqPacket
            (
            &rreqPacket,
            srcIPAddr,
            broadcastId,
            destIPAddr,
            numOfHops,
            hasBeenSent,
            forceRediscovery
            );

        bcopy( &rreqPacket, data, (NpUint32)sizeof( OdrRreqPacket ) );

        NpInt32 send_result = sendto
                                (
                                sockFd,
                                buffer,
                                ETH_FRAME_LEN,
                                0,
                                (SA *) &socket_address,
                                sizeof(socket_address)
                                );

        if ( -1 == send_result )
        {
            // Send error, may be because the destination odr
            // is not running. Continue...
            printf( "sendto error: %s. Continue...\n", strerror(errno) );
            continue;
        }

        char thisHostNode[MAX_LINE];
        char fromHostNode[MAX_LINE];
        char toHostNode[MAX_LINE];
        IPAddrToHostName( thisHostNode, GetCanonicalIPAddr( ifTable, numOfInterfaces ) );
        IPAddrToHostName( fromHostNode, rreqPacket.srcIPAddr );
        IPAddrToHostName( toHostNode, rreqPacket.destIPAddr );

        PrintOdrSendRreqMsg
            (
            thisHostNode,
            fromHostNode,
            toHostNode,
            dest_mac
            );
    }

    if( NULL != buffer )
    {
        free( buffer );
    }
}

//=============================================================================
void ProcessOdrRREQPacket
    (
    const NpInt32 odrFd,
    OdrRreqPacket *rreqPacket,
    RREQQueue *rreqQueue,
    NpInt32 *lastIndexOfRREQQueue,
    RoutingTable *routingTable,
    NpInt32 *lastIndexOfRoutingTable,
    char *srcMACAddr,
    const NpInt32 interfaceIndex,
    char *thisNodeIPAddr,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    )
{
    // Task 1: Update routing table and RREQ queue
    char MACPresentationAddr[MAC_PRESENTATION_SIZE];
    bzero( &MACPresentationAddr, MAC_PRESENTATION_SIZE );
    ToMACPresentationFormat( MACPresentationAddr, srcMACAddr );

    NpInt32 foundIndex = 0;
    QueryRREQQueueResult res = FindEntryInRREQQueue
                                (
                                &foundIndex,
                                rreqQueue,
                                // Conver to the total number of entries in queue
                                (NpUint32)(*lastIndexOfRREQQueue + 1),
                                rreqPacket->broadcastId,
                                rreqPacket->srcIPAddr
                                );

    NpBool outgoingInterfaceIndexChanged = FALSE;
    switch( res )
    {
    case RESULT_RREQ_NOT_FOUND:
        // We should add a new entry into RREQ queue.
        AddEntryToRREQQueue
            (
            rreqQueue,
            lastIndexOfRREQQueue,
            rreqPacket->broadcastId,
            rreqPacket->srcIPAddr
            );
        outgoingInterfaceIndexChanged = AddOrUpdateEntryToRoutingTable
                                            (
                                            routingTable,
                                            lastIndexOfRoutingTable,
                                            rreqPacket->srcIPAddr,
                                            rreqPacket->numOfHops,
                                            interfaceIndex,
                                            MACPresentationAddr
                                            );
        break;
    case RESULT_RREQ_SAME_BID:
        // An identical entry is found. Further check the hop
        // count to get a more efficient route.
        if( GetHopCountFromRoutingTable( routingTable, (NpUint32)(*lastIndexOfRoutingTable+1), rreqPacket->srcIPAddr ) > rreqPacket->numOfHops )
        {
            // The number of hops in the routing table is larger than that of
            // currently received RREQ. This means we have a new better
            // route.
            outgoingInterfaceIndexChanged = AddOrUpdateEntryToRoutingTable
                                                (
                                                routingTable,
                                                lastIndexOfRoutingTable,
                                                rreqPacket->srcIPAddr,
                                                rreqPacket->numOfHops,
                                                interfaceIndex,
                                                MACPresentationAddr
                                                );
        }
        else
        {
            UpdateEntryWithTimeStampInRoutingTable
                (
                routingTable,
                (NpUint32)(*lastIndexOfRoutingTable+1),
                rreqPacket->srcIPAddr
                );
        }
        break;
    case RESULT_RREQ_SMALLER_BID:
        // Update the time stamp
        UpdateEntryWithTimeStampInRoutingTable
            (
            routingTable,
            (NpUint32)(*lastIndexOfRoutingTable+1),
            rreqPacket->srcIPAddr
            );
        break;
    case RESULT_RREQ_LARGER_BID:
        UpdateEntryInRREQQueue
            (
            rreqQueue,
            foundIndex,
            rreqPacket->broadcastId
            );
        if( GetHopCountFromRoutingTable( routingTable, (NpUint32)(*lastIndexOfRoutingTable+1), rreqPacket->srcIPAddr ) > rreqPacket->numOfHops )
        {
            outgoingInterfaceIndexChanged = AddOrUpdateEntryToRoutingTable
                                                (
                                                routingTable,
                                                lastIndexOfRoutingTable,
                                                rreqPacket->srcIPAddr,
                                                rreqPacket->numOfHops,
                                                interfaceIndex,
                                                MACPresentationAddr
                                                );
        }
        break;
    default:
        err_sys( "I really don't understand why you are here..." );
        break;
    }

    // Task2: Determine to reply with RREP or to keep flooding the RREQ
    if( 0 == bcmp( rreqPacket->destIPAddr, thisNodeIPAddr, strlen( thisNodeIPAddr ) ) )
    {
        //printf( "Destination node reached!\n" );

        if( FALSE == rreqPacket->hasBeenSent )
        {
            // We can send RREP
            rreqPacket->hasBeenSent = TRUE;
            RelayRREP
                (
                odrFd,
                rreqPacket->srcIPAddr,
                srcMACAddr,
                thisNodeIPAddr,
                interfaceIndex,
                1,
                rreqPacket->forceRediscovery,
                ifTable,
                numOfInterfaces
                );
        }
    }
    else
    {
        NpInt32 outgoingInterfaceIndex = INVALID_INTERFACE_INDEX;
        char MACAddrInRoutingTable[MAC_PRESENTATION_SIZE];
        bzero( &MACAddrInRoutingTable, MAC_PRESENTATION_SIZE );
        NpUint32 indexRoutingEntryFound;
        if( TRUE == FindEntryInRoutingTable
                        (
                        &indexRoutingEntryFound,
                        &outgoingInterfaceIndex,
                        MACAddrInRoutingTable,
                        &routingTable[0],
                        (NpUint32)( *lastIndexOfRoutingTable + 1 ),
                        rreqPacket->destIPAddr
                        ) )
        {
            // This node is not the destination node, but this node
            // does have a route to the destination node.
            char RREPIPAddr[IP_SIZE];
            bzero( &RREPIPAddr, IP_SIZE );
            bcopy( rreqPacket->destIPAddr, RREPIPAddr, strlen( thisNodeIPAddr ) );

            if( TRUE == outgoingInterfaceIndexChanged || TRUE == rreqPacket->forceRediscovery )
            {
                if( RESULT_RREQ_SAME_BID != res )
                {
                    FloodRREQ
                        (
                        odrFd,
                        rreqPacket->srcIPAddr,
                        rreqPacket->broadcastId,
                        rreqPacket->destIPAddr,
                        rreqPacket->numOfHops + 1,
                        rreqPacket->hasBeenSent,
                        rreqPacket->forceRediscovery,
                        interfaceIndex,
                        ifTable,
                        numOfInterfaces
                        );
                }
            }
            else
            {
                if( FALSE == rreqPacket->hasBeenSent && FALSE == rreqPacket->forceRediscovery )
                {
                    // We can send RREP
                    rreqPacket->hasBeenSent = TRUE;
                    RelayRREP
                        (
                        odrFd,
                        rreqPacket->srcIPAddr,
                        srcMACAddr,
                        rreqPacket->destIPAddr,
                        interfaceIndex,
                        routingTable[indexRoutingEntryFound].numOfHops + 1,
                        rreqPacket->forceRediscovery,
                        ifTable,
                        numOfInterfaces
                        );
                }
            }
        }
        else
        {
            //printf( "An intermediate node. Keep flooding RREQ\n" );
            // This node neither is the destination nor has a route to the
            // destination node. So we need to keep flooding RREQ.
            FloodRREQ
                (
                odrFd,
                rreqPacket->srcIPAddr,
                rreqPacket->broadcastId,
                rreqPacket->destIPAddr,
                rreqPacket->numOfHops + 1,
                rreqPacket->hasBeenSent,
                rreqPacket->forceRediscovery,
                interfaceIndex,
                ifTable,
                numOfInterfaces
                );
        }
    }
}

//=============================================================================
void ProcessOdrRREPPacket
    (
    const NpInt32 odrFd,
    NpUint32 *globalBroadcastId,
    OdrRrepPacket *rrepPacket,
    RREQQueue *rreqQueue,
    NpInt32 *lastIndexOfRREQQueue,
    RoutingTable *routingTable,
    NpInt32 *lastIndexOfRoutingTable,
    char *srcMACAddr,
    const NpInt32 interfaceIndex,
    char *thisNodeIPAddr,
    OdrAppMsgPacket *appMsgQueue,
    NpInt32 *lastIndexOfAppMsgQueue,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    )
{
    static OdrRrepPacket preqQueue[MAX_RREP_PER_ODR];   // Note this is static
    static NpUint32 queuedRreqCount = 0;                // Note this is static
    if( 0 == bcmp( rrepPacket->srcIPAddr, thisNodeIPAddr, strlen( thisNodeIPAddr ) ) )
    {
        return;
    }

    char srcMACPresentationAddr[MAC_PRESENTATION_SIZE];
    bzero( &srcMACPresentationAddr, MAC_PRESENTATION_SIZE );
    ToMACPresentationFormat( srcMACPresentationAddr, srcMACAddr );

    NpInt32 outgoingInterfaceIndex = INVALID_INTERFACE_INDEX;
    char MACAddrInRoutingTable[MAC_PRESENTATION_SIZE];
    bzero( &MACAddrInRoutingTable, MAC_PRESENTATION_SIZE );
    NpUint32 indexRoutingEntryFound;
    NpBool routingEntryFound = FindEntryInRoutingTable
                                    (
                                    &indexRoutingEntryFound,
                                    &outgoingInterfaceIndex,
                                    MACAddrInRoutingTable,
                                    &routingTable[0],
                                    (NpUint32)(*lastIndexOfRoutingTable + 1),
                                    rrepPacket->srcIPAddr
                                    );

    // Task1: Update the routing table
    if( TRUE == routingEntryFound )
    {
        if( TRUE == rrepPacket->forceRediscovery || GetHopCountFromRoutingTable( routingTable, (NpUint32)(*lastIndexOfRoutingTable+1), rrepPacket->srcIPAddr ) > rrepPacket->numOfHops )
        {
            // The number of hops in the routing table is larger than that of
            // currently received RREQ. This means we have a new better
            // route.
            AddOrUpdateEntryToRoutingTable
                (
                routingTable,
                lastIndexOfRoutingTable,
                rrepPacket->srcIPAddr,
                rrepPacket->numOfHops,
                interfaceIndex,
                srcMACPresentationAddr
                );
        }
        else
        {
            UpdateEntryWithTimeStampInRoutingTable
                (
                routingTable,
                (NpUint32)(*lastIndexOfRoutingTable+1),
                rrepPacket->srcIPAddr
                );
        }
    }
    else
    {
        AddOrUpdateEntryToRoutingTable
            (
            routingTable,
            lastIndexOfRoutingTable,
            rrepPacket->srcIPAddr,
            rrepPacket->numOfHops,
            interfaceIndex,
            srcMACPresentationAddr
            );
    }

    // Task 2: Reset the variables so they can be used again.
    {
        // Reset outgoingInterfaceIndex and MACAddrInRoutingTable
        // so that we can re-use them.
        bzero( &MACAddrInRoutingTable, MAC_PRESENTATION_SIZE );
        outgoingInterfaceIndex = INVALID_INTERFACE_INDEX;
    }

    // Task 3: Make a reaction.
    if( 0 == bcmp( rrepPacket->destIPAddr, thisNodeIPAddr, strlen( thisNodeIPAddr ) ) )
    {
        FlushAppMsgQueue
            (
            odrFd,
            appMsgQueue,
            lastIndexOfAppMsgQueue,
            rrepPacket->srcIPAddr,
            srcMACAddr,
            interfaceIndex,
            ifTable,
            numOfInterfaces
            );

        FlushRrepQueue
            (
            odrFd,
            preqQueue,
            &queuedRreqCount,
            rrepPacket->srcIPAddr,
            srcMACAddr,
            interfaceIndex,
            ifTable,
            numOfInterfaces
            );
    }
    else
    {
        // This is an intermediate node. However, we cannot just propagate
        // the RREP. We need to consider the case where a routing entry
        // (to the destination) is expired. In this case, we should queue
        // the RREP and then flood RREQ.
        routingEntryFound = FindEntryInRoutingTable
                                (
                                &indexRoutingEntryFound,
                                &outgoingInterfaceIndex,
                                MACAddrInRoutingTable,
                                &routingTable[0],
                                (NpUint32)(*lastIndexOfRoutingTable + 1),
                                rrepPacket->destIPAddr
                                );
        if( TRUE == routingEntryFound )
        {
            // Good. Propagate the RREP.
            char succinctMACAddrInRoutingTable[MAC_SIZE];
            bzero( &succinctMACAddrInRoutingTable, MAC_SIZE );
            ToMACSuccinctFormat( succinctMACAddrInRoutingTable, MACAddrInRoutingTable );

            RelayRREP
                (
                odrFd,
                rrepPacket->destIPAddr,
                succinctMACAddrInRoutingTable,
                rrepPacket->srcIPAddr,
                outgoingInterfaceIndex,
                rrepPacket->numOfHops + 1,
                rrepPacket->forceRediscovery,
                ifTable,
                numOfInterfaces
                );
        }
        else
        {
            FloodRREQ
                (
                odrFd,
                thisNodeIPAddr,
                *globalBroadcastId,
                rrepPacket->destIPAddr,
                1,
                FALSE,
                FALSE,
                INVALID_INTERFACE_INDEX,
                ifTable,
                numOfInterfaces
                );

            AddRrepPacketToRrepQueue( &preqQueue[0], &queuedRreqCount, rrepPacket );

            // Important! Don't forget to increment the global broadcast ID.
            *globalBroadcastId = *globalBroadcastId + 1;
        }
    }
}

//=============================================================================
void ProcessOdrAppMsgPacket
    (
    const NpInt32 sockFd,
    const NpInt32 odrFd,
    OdrAppMsgPacket *appMsgPacket,
    OdrAppMsgPacket *appMsgQueue,
    NpInt32 *lastIndexOfAppMsgQueue,
    NpUint32 *globalBroadcastId,
    RoutingTable *routingTable,
    NpInt32 *lastIndexOfRoutingTable,
    char *srcMACAddr,
    const NpInt32 interfaceIndex,
    char *thisNodeIPAddr,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces,
    PathPortTable *ppTable
    )
{
    char srcMACPresentationAddr[MAC_PRESENTATION_SIZE];
    bzero( &srcMACPresentationAddr, MAC_PRESENTATION_SIZE );
    ToMACPresentationFormat( srcMACPresentationAddr, srcMACAddr );

    NpInt32 outgoingInterfaceIndex = INVALID_INTERFACE_INDEX;
    char MACAddrInRoutingTable[MAC_PRESENTATION_SIZE];
    bzero( &MACAddrInRoutingTable, MAC_PRESENTATION_SIZE );

    NpUint32 indexRoutingEntryFound;
    NpBool routingEntryFound = FindEntryInRoutingTable
                                        (
                                        &indexRoutingEntryFound,
                                        &outgoingInterfaceIndex,
                                        MACAddrInRoutingTable,
                                        &routingTable[0],
                                        (NpUint32)(*lastIndexOfRoutingTable + 1),
                                        appMsgPacket->srcIPAddr
                                        );

    // Task1: Update the routing table
    if( TRUE == routingEntryFound )
    {
        UpdateEntryWithTimeStampInRoutingTable
            (
            routingTable,
            (NpUint32)(*lastIndexOfRoutingTable+1),
            appMsgPacket->srcIPAddr
            );
    }
    else
    {
        AddOrUpdateEntryToRoutingTable
            (
            routingTable,
            lastIndexOfRoutingTable,
            appMsgPacket->srcIPAddr,
            appMsgPacket->numOfHops,
            interfaceIndex,
            srcMACPresentationAddr
            );
    }

    // Task 2: Reset the variables so they can be used again.
    {
        // Reset outgoingInterfaceIndex and MACAddrInRoutingTable
        // so that we can re-use them.
        bzero( &MACAddrInRoutingTable, MAC_PRESENTATION_SIZE );
        outgoingInterfaceIndex = INVALID_INTERFACE_INDEX;
    }

    // Task 3: Make a reaction (deliver the AppMsg packet or flood the RREQ )
    if( 0 == bcmp( appMsgPacket->destIPAddr, thisNodeIPAddr, strlen( thisNodeIPAddr ) ) )
    {
        // Good. Deliver the message to the application (server/client).
        Packet packetData;
        InitPacketData( &packetData );
        BuildPacketData
            (
            &packetData,
            appMsgPacket->payload.message,
            appMsgPacket->srcIPAddr,
            appMsgPacket->srcPort,
            -1 // Just give the flag '1'. It is of no use. Any number is okay.
            );

        struct sockaddr_un toAddr;
        char sentSunPath[SUN_PATH_SIZE];
        bzero( &toAddr, sizeof( toAddr ) );
        bzero( &sentSunPath, SUN_PATH_SIZE );
        toAddr.sun_family = AF_LOCAL;

        GetPathFromPathPortTable
            (
            &sentSunPath[0],
            ppTable,
            appMsgPacket->payload.port
            );

        strcpy( toAddr.sun_path, sentSunPath );
        //DebugPathPortTable( ppTable );
        //printf( "Send to: %s(%d)\n", toAddr.sun_path, appMsgPacket->payload.port );
        SendMsgToApplication
            (
            sockFd,
            (char *)&packetData,
            sizeof( Packet ),
            (SA *)&toAddr,
            sizeof(toAddr)
            );
    }
    else
    {
        routingEntryFound = FindEntryInRoutingTable
                                (
                                &indexRoutingEntryFound,
                                &outgoingInterfaceIndex,
                                MACAddrInRoutingTable,
                                &routingTable[0],
                                (NpUint32)(*lastIndexOfRoutingTable + 1),
                                appMsgPacket->destIPAddr
                                );
        if( TRUE == routingEntryFound )
        {
            // Propagate the AppMsg packet
            char succinctMACAddrInRoutingTable[MAC_SIZE];
            bzero( &succinctMACAddrInRoutingTable, MAC_SIZE );
            ToMACSuccinctFormat( succinctMACAddrInRoutingTable, MACAddrInRoutingTable );
            DeliverApplicationMessage
                (
                odrFd,
                appMsgPacket->payload.message,
                appMsgPacket->destIPAddr,
                appMsgPacket->destPort,
                succinctMACAddrInRoutingTable,
                appMsgPacket->srcIPAddr,
                appMsgPacket->srcPort,
                outgoingInterfaceIndex,
                appMsgPacket->numOfHops + 1,
                ifTable,
                numOfInterfaces
                );
        }
        else
        {
            // The route entry is expired. ODR needs to
            // put AppMsg message into queue and then
            // flood RREQ.
            AddEntryToAppMsgQueue
                (
                appMsgQueue,
                lastIndexOfAppMsgQueue,
                appMsgPacket
                );

            FloodRREQ
                (
                odrFd,
                thisNodeIPAddr,
                *globalBroadcastId,
                appMsgPacket->destIPAddr,
                1,
                FALSE,
                FALSE,
                INVALID_INTERFACE_INDEX,
                ifTable,
                numOfInterfaces
                );

                // Important! Don't forget to increment the global broadcast ID.
                *globalBroadcastId = *globalBroadcastId + 1;
        }
    }
}

//=============================================================================
void RelayRREP
    (
    const NpInt32 sockFd,
    char *destIPAddr,
    char *destMACAddr,
    char *srcIPAddr,
    const NpInt32 interfaceIndex,
    const NpUint32 numOfHops,
    NpBool forceRediscovery,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    )
{
    // Adapted from: http://aschauf.landshut.org/fh/linux/udp_vs_raw/ch01s03.html#id2766267

    void* buffer = (void*)calloc(ETH_FRAME_LEN,1);
    unsigned char* etherhead = buffer;
    unsigned char* data = buffer + ETHER_FRAME_HEADER_SIZE;

    /*another pointer to ethernet header*/
    struct ethhdr *eh = (struct ethhdr *)etherhead;

    /* prepare sockaddr_ll */
    struct sockaddr_ll socket_address; /*target address*/
    socket_address.sll_family = PF_PACKET;
    socket_address.sll_protocol = htons( ETH_P_IP );
    socket_address.sll_hatype = ARPHRD_ETHER;
    socket_address.sll_pkttype = PACKET_OTHERHOST;
    socket_address.sll_halen = ETH_ALEN;

    NpUint32 i = 0;
    for( ; i < numOfInterfaces; ++i )
    {
        if( interfaceIndex == ifTable[i].interfaceIndex )
        {
            ToMACSuccinctFormat( socket_address.sll_addr, ifTable[i].hwAddr );
            socket_address.sll_addr[6] = 0x00;
            socket_address.sll_addr[7] = 0x00;

            socket_address.sll_ifindex = ifTable[i].interfaceIndex;

            /*char pres[MAC_PRESENTATION_SIZE];
            bzero( &pres,MAC_PRESENTATION_SIZE );
            ToMACPresentationFormat( pres, destMACAddr );
            printf( "In Relay RREP: MAC addr(%s)\n", pres );*/

            memcpy((void*)buffer, (void*)destMACAddr, ETH_ALEN);
            memcpy((void*)(buffer+ETH_ALEN), (void*)socket_address.sll_addr, ETH_ALEN);
            eh->h_proto = htons( MY_PROTOCOL );

            // Build RREP packet
            OdrRrepPacket rrepPacket;
            InitOdrRrepPacket( &rrepPacket );
            BuildOdrRrepPacket
                (
                &rrepPacket,
                srcIPAddr,
                destIPAddr,
                numOfHops,
                forceRediscovery
                );

            bcopy( &rrepPacket, data, (NpUint32)sizeof( OdrRrepPacket ) );

            NpInt32 send_result = sendto
                                    (
                                    sockFd,
                                    buffer,
                                    ETH_FRAME_LEN,
                                    0,
                                    (SA *) &socket_address,
                                    sizeof(socket_address)
                                    );

            if ( -1 == send_result )
            {
                // Send error, may be because the destination odr
                // is not running. Continue...
                printf( "sendto error: %s. Continue...\n", strerror(errno) );
                continue;
            }

            char thisHostNode[MAX_LINE];
            char fromHostNode[MAX_LINE];
            char toHostNode[MAX_LINE];
            IPAddrToHostName( thisHostNode, GetCanonicalIPAddr( ifTable, numOfInterfaces ) );
            IPAddrToHostName( fromHostNode, rrepPacket.srcIPAddr );
            IPAddrToHostName( toHostNode, rrepPacket.destIPAddr );

            PrintOdrSendRrepMsg
                (
                thisHostNode,
                fromHostNode,
                toHostNode,
                destMACAddr
                );
        }
    }


    if( NULL != buffer )
    {
        free( buffer );
    }
}

//=============================================================================
void DeliverApplicationMessage
    (
    const NpInt32 odrFd,
    char *payload,
    char *destIPAddr,
    const NpInt32 destPort,
    char *destMACAddr,
    char *srcIPAddr,
    const NpInt32 srcPort,
    const NpInt32 interfaceIndex,
    const NpUint32 numOfHops,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    )
{
    // Adapted from: http://aschauf.landshut.org/fh/linux/udp_vs_raw/ch01s03.html#id2766267

    void* buffer = (void*)calloc(ETH_FRAME_LEN,1);
    unsigned char* etherhead = buffer;
    unsigned char* data = buffer + ETHER_FRAME_HEADER_SIZE;

    /*another pointer to ethernet header*/
    struct ethhdr *eh = (struct ethhdr *)etherhead;
    /* prepare sockaddr_ll */
    struct sockaddr_ll socket_address; /*target address*/
    socket_address.sll_family = PF_PACKET;
    socket_address.sll_protocol = htons( ETH_P_IP );
    socket_address.sll_hatype = ARPHRD_ETHER;
    socket_address.sll_pkttype = PACKET_OTHERHOST;
    socket_address.sll_halen = ETH_ALEN;

    NpUint32 i = 0;
    for( ; i < numOfInterfaces; ++i )
    {
        if( interfaceIndex == ifTable[i].interfaceIndex )
        {
            ToMACSuccinctFormat( socket_address.sll_addr, ifTable[i].hwAddr );
            socket_address.sll_addr[6] = 0x00;
            socket_address.sll_addr[7] = 0x00;

            socket_address.sll_ifindex = ifTable[i].interfaceIndex;

            /*char pres[MAC_PRESENTATION_SIZE];
            bzero( &pres,MAC_PRESENTATION_SIZE );
            ToMACPresentationFormat( pres, destMACAddr );*/

            memcpy((void*)buffer, (void*)destMACAddr, ETH_ALEN);
            memcpy((void*)(buffer+ETH_ALEN), (void*)socket_address.sll_addr, ETH_ALEN);
            eh->h_proto = htons( MY_PROTOCOL );

            // Build AppMsg packet
            Packet packet;
            InitPacketData( &packet );
            BuildPacketData
                (
                &packet,
                payload,
                destIPAddr,
                destPort,
                0
                );

            OdrAppMsgPacket appMsgPacket;
            InitOdrAppMsgPacket( &appMsgPacket );
            BuildOdrAppMsgPacket
                (
                &appMsgPacket,
                destIPAddr,
                destPort,
                srcIPAddr,
                srcPort,
                numOfHops,
                sizeof( Packet ),
                &packet
                );

            bcopy( &appMsgPacket, data, (NpUint32)sizeof( OdrAppMsgPacket ) );

            NpInt32 send_result = sendto
                                    (
                                    odrFd,
                                    buffer,
                                    ETH_FRAME_LEN,
                                    0,
                                    (SA *) &socket_address,
                                    sizeof(socket_address)
                                    );

            if ( -1 == send_result )
            {
                // Send error, may be because the destination odr
                // is not running. Continue...
                printf( "sendto error: %s. Continue...\n", strerror(errno) );
                continue;
            }

            char thisHostNode[MAX_LINE];
            char fromHostNode[MAX_LINE];
            char toHostNode[MAX_LINE];
            IPAddrToHostName( thisHostNode, GetCanonicalIPAddr( ifTable, numOfInterfaces ) );
            IPAddrToHostName( fromHostNode, appMsgPacket.srcIPAddr );
            IPAddrToHostName( toHostNode, appMsgPacket.destIPAddr );

            PrintOdrSendAppMsgMsg
                (
                thisHostNode,
                fromHostNode,
                toHostNode,
                destMACAddr,
                payload
                );
        }
    }
}

//=============================================================================
char *GetCanonicalIPAddr( NeworkInterfacesTable *table, const NpUint32 count )
{
    NpUint32 i = 0;
    for( ; i < count; ++i )
    {
        if( 0 == bcmp( table[i].name, "eth0", strlen( "eth0" ) ) )
        {
            return table[i].IPAddr;
        }
    }
}

//=============================================================================
void SendMsgToApplication
    (
    const NpInt32 sockFd,
    char *buffer,
    const NpInt32 bufLen,
    SA *sa,
    const NpInt32 addrLen
    )
{
    printf( "Send message to the application\n" );
    NpInt32 nSent = sendto
                        (
                        sockFd,
                        buffer,
                        bufLen,
                        0,
                        sa,
                        addrLen
                        );

    if( nSent < 0 )
    {
        // This might happen because the application is not running.
        // However, in this case we don't want to terminate the odr
        // process since other applications may be actively, currently
        // use our odr process.
        err_ret( "The corresponding application is not running. Continue..." );
    }
}

//=============================================================================
void FlushAppMsgQueue
    (
    const NpInt32 odrFd,
    OdrAppMsgPacket *queue,
    NpInt32 *lastIndexOfAppMsgQueue,
    char *srcIPAddr,
    char *srcMACAddr,
    const NpInt32 interfaceIndex,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    )
{
    NpInt32 i = 0;
    const NpInt32 count = (*lastIndexOfAppMsgQueue) + 1;
    for( ; i < count; ++i )
    {
        if( 0 == bcmp( queue[i].destIPAddr, srcIPAddr, strlen( srcIPAddr ) ) )
        {
            DeliverApplicationMessage
                (
                odrFd,
                queue[i].payload.message,
                queue[i].destIPAddr,
                queue[i].destPort,
                srcMACAddr,
                queue[i].srcIPAddr,
                queue[i].srcPort,
                interfaceIndex,
                queue[i].numOfHops + 1,
                ifTable,
                numOfInterfaces
                );

            RemoveAppMsgPacketAndShrinkAppMsgQueue
                (
                queue,
                lastIndexOfAppMsgQueue,
                i
                );
        }
    }
}

//=============================================================================
void FlushRrepQueue
    (
    const NpInt32 odrFd,
    OdrRrepPacket *queue,
    NpUint32 *count,
    char *srcIPAddr,
    char *srcMACAddr,
    const NpInt32 interfaceIndex,
    NeworkInterfacesTable *ifTable,
    const NpUint32 numOfInterfaces
    )
{
    NpUint32 i = 0;
    for( ; i < *count; ++i )
    {
        if( 0 == bcmp( queue[i].destIPAddr, srcIPAddr, strlen( srcIPAddr ) ) )
        {
            RelayRREP
                (
                odrFd,
                queue[i].destIPAddr,
                srcMACAddr,
                queue[i].srcIPAddr,
                interfaceIndex,
                queue[i].numOfHops + 1,
                queue[i].forceRediscovery,
                ifTable,
                numOfInterfaces
                );

            RemoveRrepPacketAndShrinkRrepQueue
                (
                queue,
                count,
                i
                );
        }
    }
}

//=============================================================================
void AddRrepPacketToRrepQueue
    (
    OdrRrepPacket *queue,
    NpUint32 *count,
    OdrRrepPacket *packetToBeAdded
    )
{
    if( *count >= MAX_RREP_PER_ODR )
    {
        err_sys( "Unable to add entry to RREP queue. The queue is full" );
    }

    InitOdrRrepPacket( &queue[*count] );
    CopyRrepPacket( &queue[*count], packetToBeAdded );

    *count = *count + 1;
}

//=============================================================================
void RemoveRrepPacketAndShrinkRrepQueue
    (
    OdrRrepPacket *queue,
    NpUint32 *count,
    const NpUint32 indexRemoved
    )
{
    if( indexRemoved >= *count )
    {
        err_sys( "The RREP packet that needs removing is not in the queue. Exit..." );
    }

    if( 0 == *count )
    {
        err_sys( "Why do you want to remove an RREP from an empty queue...? Exit..." );
    }

    NpUint32 movedCandidate = *count - 1;
    if( movedCandidate > indexRemoved )
    {
        CopyRrepPacket( &queue[indexRemoved], &queue[movedCandidate] );
    }

    *count = *count - 1;
}

