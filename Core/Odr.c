#include "unp.h"
#include <stdlib.h>
#include <sys/socket.h>
#include <netpacket/packet.h>
#include <net/ethernet.h> /* the L2 protocols */
#include <linux/if_ether.h>
//#include <linux/if_packet.h>
#include "OdrTasks.h"
#include "NetworkInterface.h"
#include "RoutingTable.h"
#include "OdrPacket.h"
#include "CommonType.h"
#include "Config.h"
#include "NpType.h"

#ifdef DEBUG_ROUTING_TABLE_AND_PACKETS
#undef DEBUG_ROUTING_TABLE_AND_PACKETS
#endif

// Uncomment this if you want to debug the routing
// table and the received packets.
//#define DEBUG_ROUTING_TABLE_AND_PACKETS

static const NpInt32 INITIAL_CLIENT_PORT = 51415;
static const NpInt32 MAX_PORT_NUMBER = 2147483648; // (2^31) - 1

char g_CanonicalIPAddr[IP_SIZE];


void IncrementPortNumber( NpInt32 *portNumber );

NpInt32 main( NpInt32 argc, char *argv[] )
{
    if( 2 != argc )
    {
        err_sys("Usage: ./Odr <staleness> \n");
    }
    unlink( ODR_PATH );

    // Init serveral tables
    PathPortTable ppTable[MAX_APP_PER_ODR];
    NeworkInterfacesTable netIfTable[MAX_INTERFACE_PER_NODE];
    InitPathPortTable( &ppTable[0] );
    const NpUint32 numOfInterfaces = InitNetworkInterfaceTable( &netIfTable[0]);
    DebugNetworkInterfaceTable( &netIfTable[0], numOfInterfaces );
    RoutingTable routingTable[MAX_ENTRY_PER_ROUTING_TABLE];
    InitRoutingTable( &routingTable[0] );
    NpInt32 lastIndexOfRoutingTable = -1;
    RREQQueue rreqQueue[MAX_RREQ_PER_ODR];
    InitRREQQueue( &rreqQueue[0] );
    NpInt32 lastIndexOfRREQQueue = -1;
    OdrAppMsgPacket appMsgQueue[MAX_APPMSG_PER_ODR];
    NpInt32 lastIndexOfAppMsgQueue = -1;
    static NpInt32 currentTempClientPort = INITIAL_CLIENT_PORT;
    NpUint32 broadcastID = 0;

    // Initialize global variables
    bzero( g_CanonicalIPAddr, IP_SIZE );
    char *IPStr = GetCanonicalIPAddr( &netIfTable[0], numOfInterfaces );
    bcopy( IPStr, g_CanonicalIPAddr, strlen( IPStr ) );
    printf( "IP address: %s(%d)\n", g_CanonicalIPAddr, (NpInt32)strlen( g_CanonicalIPAddr ) );

    NpInt32 staleness = atoi( argv[1] );

    NpInt32 sockFd = 0; // Socket for communicating with application
    NpInt32 odrFd = 0;  // Socket for communicating with other odrs
    struct sockaddr_un thisAddr;
    bzero( &thisAddr, sizeof( thisAddr ) );

    // Setup socket for applicaton-odr communication
    sockFd = Socket( AF_LOCAL, SOCK_DGRAM, 0 );
    thisAddr.sun_family = AF_LOCAL;
    strcpy( thisAddr.sun_path, ODR_PATH );
    Bind( sockFd, (SA *) &thisAddr, sizeof( thisAddr ) );

    // Setup socket for odr-odr communication
    odrFd = Socket( PF_PACKET, SOCK_RAW, htons( MY_PROTOCOL ) );

    // Add well-known port into PathPort table
    AddEntry2PathPortTable
                (
                &ppTable[0],
                SERVER_PATH,
                SERVER_PORT
                );
    //DebugPathPortTable( &ppTable[0] );

    fd_set rset, allrset;
    FD_ZERO( &allrset );
    FD_SET( sockFd, &allrset );
    FD_SET( odrFd, &allrset );
    NpInt32 maxfd = max( sockFd, odrFd );
    for( ; ; )
    {
        rset = allrset;
    #ifdef DEBUG_ROUTING_TABLE_AND_PACKETS
        DebugRoutingTable( &routingTable[0], (NpUint32)(lastIndexOfRoutingTable+1) );
    #endif
        MySelect( (maxfd+1), &rset, NULL, NULL, NULL );

        if( FD_ISSET( sockFd, &rset ) )
        {
            struct sockaddr_un fromAddr;
            bzero( &fromAddr, sizeof( fromAddr ) );
            NpInt32 len = sizeof( fromAddr );

            Packet receivedPacketData;
            bzero( &receivedPacketData, sizeof( Packet ) );
            const NpUint32 nRead = Recvfrom
                                    (
                                    sockFd,
                                    (char *) &receivedPacketData,
                                    sizeof( Packet ),
                                    0,
                                    (SA *) &fromAddr,
                                    &len
                                    );

        #ifdef DEBUG_ROUTING_TABLE_AND_PACKETS
            //DebugPacketData( &receivedPacketData );
        #endif

            // First set invalid those entries that are expired
            // or we force the odr to rediscover.
            if( 1 == receivedPacketData.flag )
            {
                SetRoutingEntriesInvalid
                    (
                    &routingTable[0],
                    (NpUint32)(lastIndexOfRoutingTable + 1),
                    receivedPacketData.IPAddr
                    );
            }

            SetRoutingEntriesInvalidIfNeeded
                (
                &routingTable[0],
                (NpUint32)(lastIndexOfRoutingTable + 1),
                receivedPacketData.IPAddr,
                staleness
                );

            NpInt32 portNumInPPTable;
            const NpBool portExist = GetPortFromPathPortTable
                                        (
                                        &portNumInPPTable,
                                        &ppTable[0],
                                        fromAddr.sun_path
                                        );

            if( FALSE == portExist )
            {
                // We need to add an entry into PathPortTable
                AddEntry2PathPortTable( &ppTable[0], fromAddr.sun_path, currentTempClientPort );
                portNumInPPTable = currentTempClientPort;
                IncrementPortNumber( &currentTempClientPort );
            }

            struct sockaddr_un toAddr;
            char sentSunPath[SUN_PATH_SIZE];
            bzero( &toAddr, sizeof( toAddr ) );
            bzero( &sentSunPath, SUN_PATH_SIZE );
            toAddr.sun_family = AF_LOCAL;
            GetPathFromPathPortTable
                (
                &sentSunPath[0],
                &ppTable[0],
                receivedPacketData.port
                );
            bcopy( sentSunPath, toAddr.sun_path, strlen( sentSunPath ) );

            Packet sentPacketData;
            bzero( &sentPacketData, sizeof( Packet ) );
            BuildPacketData
                (
                &sentPacketData,
                receivedPacketData.message,
                g_CanonicalIPAddr,
                portNumInPPTable,
                receivedPacketData.flag
                );

        #ifdef DEBUG_ROUTING_TABLE_AND_PACKETS
            //DebugPacketData( &sentPacketData );
        #endif

            if( 0 == bcmp( receivedPacketData.IPAddr, g_CanonicalIPAddr, strlen(g_CanonicalIPAddr) ) )
            {
                // Oh, send msg to the application.
                //DebugPathPortTable( &ppTable[0] );
                //printf("Send to client: %s\n", sentSunPath );
                SendMsgToApplication
                    (
                    sockFd,
                    (char *)&sentPacketData,
                    sizeof( Packet ),
                    (SA *)&toAddr,
                    sizeof(toAddr)
                    );
            }
            else
            {
                // Need to forward the message to other nodes
                NpInt32 outgoingInterfaceIndex = 0;
                char srcMacAddr[MAC_PRESENTATION_SIZE];
                bzero( &srcMacAddr, MAC_PRESENTATION_SIZE );

                NpUint32 indexRoutingEntryFound;
                if( TRUE == FindEntryInRoutingTable
                                (
                                &indexRoutingEntryFound,
                                &outgoingInterfaceIndex,
                                srcMacAddr,
                                &routingTable[0],
                                (NpUint32)( lastIndexOfRoutingTable + 1 ),
                                receivedPacketData.IPAddr
                                ) )
                {
                    // The entry is already in the routing table
                    char succinctMACAddrInRoutingTable[MAC_SIZE];
                    bzero( &succinctMACAddrInRoutingTable, MAC_SIZE );
                    ToMACSuccinctFormat( succinctMACAddrInRoutingTable, srcMacAddr );
                    DeliverApplicationMessage
                        (
                        odrFd,
                        receivedPacketData.message,
                        receivedPacketData.IPAddr,
                        receivedPacketData.port,
                        succinctMACAddrInRoutingTable,
                        g_CanonicalIPAddr,
                        portNumInPPTable,
                        outgoingInterfaceIndex,
                        1,
                        netIfTable,
                        numOfInterfaces
                        );
                }
                else
                {
                    // No corresponding entry is found. We need to add
                    // the application message to queue and flood RREQ.
                    // Once an desired RREP is received, we will flush the
                    // application message queue.

                    OdrAppMsgPacket appMsgPacket;
                    BuildOdrAppMsgPacket
                        (
                        &appMsgPacket,
                        receivedPacketData.IPAddr,
                        receivedPacketData.port,
                        g_CanonicalIPAddr,
                        portNumInPPTable,
                        1,
                        sizeof( Packet ),
                        &sentPacketData
                        );

                    AddEntryToAppMsgQueue
                        (
                        &appMsgQueue[0],
                        &lastIndexOfAppMsgQueue,
                        &appMsgPacket
                        );

                    FloodRREQ
                        (
                        odrFd,
                        g_CanonicalIPAddr,
                        broadcastID,
                        receivedPacketData.IPAddr,
                        1,
                        FALSE,
                        receivedPacketData.flag,
                        INVALID_INTERFACE_INDEX,
                        netIfTable,
                        numOfInterfaces
                        );
                    ++broadcastID;
                }
            }
        }

        if( FD_ISSET( odrFd, &rset ) )
        {
            //printf("Receive message from ODR socket.\n");

            NpUchar buffer[ETH_FRAME_LEN];
            struct sockaddr_ll fromAddr;
            NpUint32 length = sizeof( fromAddr );
            NpInt32 nRead = Recvfrom
                                (
                                odrFd,
                                buffer,
                                ETH_FRAME_LEN,
                                0,
                                (SA *)&fromAddr,
                                &length
                                );

            NpUchar *data = buffer + ETHER_FRAME_HEADER_SIZE;
            printf( "This IP (%s) received: ", g_CanonicalIPAddr );
            ODRPacketType type = DetermineODRPacketType( (NpInt32)data[0] );
            if( ODR_RREQ == type )
            {
                OdrRreqPacket packet;
                InitOdrRreqPacket( &packet );
                bcopy( data, &packet, (NpUint32)sizeof( OdrRreqPacket ) );

                if( 0 == bcmp( packet.srcIPAddr, g_CanonicalIPAddr, strlen( g_CanonicalIPAddr ) ) )
                {
                    continue;
                }

            #ifdef DEBUG_ROUTING_TABLE_AND_PACKETS
                DebugOdrRreqPacket( &packet );
            #endif

                // First update the routing table so that an
                // expired entry is set invalid.
                SetRoutingEntriesInvalidIfNeeded
                    (
                    &routingTable[0],
                    (NpUint32)(lastIndexOfRoutingTable + 1),
                    packet.srcIPAddr,
                    staleness
                );
                SetRoutingEntriesInvalidIfNeeded
                    (
                    &routingTable[0],
                    (NpUint32)(lastIndexOfRoutingTable + 1),
                    packet.destIPAddr,
                    staleness
                );

                ProcessOdrRREQPacket
                    (
                    odrFd,
                    &packet,
                    &rreqQueue[0],
                    &lastIndexOfRREQQueue,
                    &routingTable[0],
                    &lastIndexOfRoutingTable,
                    fromAddr.sll_addr,
                    fromAddr.sll_ifindex,
                    g_CanonicalIPAddr,
                    netIfTable,
                    numOfInterfaces
                    );
            }
            else if( ODR_RREP == type )
            {
                OdrRrepPacket packet;
                InitOdrRrepPacket( &packet );
                bcopy( data, &packet, (NpUint32)sizeof( OdrRrepPacket ) );

            #ifdef DEBUG_ROUTING_TABLE_AND_PACKETS
                DebugOdrRrepPacket( &packet );
            #endif

                // First update the routing table so that an
                // expired entry is set invalid.
                SetRoutingEntriesInvalidIfNeeded
                    (
                    &routingTable[0],
                    (NpUint32)(lastIndexOfRoutingTable + 1),
                    packet.srcIPAddr,
                    staleness
                );
                SetRoutingEntriesInvalidIfNeeded
                    (
                    &routingTable[0],
                    (NpUint32)(lastIndexOfRoutingTable + 1),
                    packet.destIPAddr,
                    staleness
                );

                ProcessOdrRREPPacket
                    (
                    odrFd,
                    &broadcastID, // Note that this variable may be modified
                    &packet,
                    &rreqQueue[0],
                    &lastIndexOfRREQQueue,
                    &routingTable[0],
                    &lastIndexOfRoutingTable,
                    fromAddr.sll_addr,
                    fromAddr.sll_ifindex,
                    g_CanonicalIPAddr,
                    &appMsgQueue[0],
                    &lastIndexOfAppMsgQueue,
                    netIfTable,
                    numOfInterfaces
                    );
            }
            else if( ODR_APPMSG == type )
            {
                OdrAppMsgPacket packet;
                InitOdrAppMsgPacket( &packet );
                bcopy( data, &packet, (NpUint32)sizeof( OdrAppMsgPacket ) );

           #ifdef DEBUG_ROUTING_TABLE_AND_PACKETS
                DebugOdrAppMsgPacket( &packet );
           #endif

                // First update the routing table so that an
                // expired entry is set invalid.
                SetRoutingEntriesInvalidIfNeeded
                    (
                    &routingTable[0],
                    (NpUint32)(lastIndexOfRoutingTable + 1),
                    packet.srcIPAddr,
                    staleness
                );
                SetRoutingEntriesInvalidIfNeeded
                    (
                    &routingTable[0],
                    (NpUint32)(lastIndexOfRoutingTable + 1),
                    packet.destIPAddr,
                    staleness
                );

                ProcessOdrAppMsgPacket
                    (
                    sockFd,
                    odrFd,
                    &packet,
                    &appMsgQueue[0],
                    &lastIndexOfAppMsgQueue,
                    &broadcastID,
                    &routingTable[0],
                    &lastIndexOfRoutingTable,
                    fromAddr.sll_addr,
                    fromAddr.sll_ifindex,
                    g_CanonicalIPAddr,
                    netIfTable,
                    numOfInterfaces,
                    &ppTable[0]
                    );
            }
            else
            {
                err_sys( "Unknown packet type. Exit..." );
            }
        }
    }

    return 0;
}

void IncrementPortNumber( NpInt32 *portNumber )
{
    if( MAX_PORT_NUMBER == *portNumber )
    {
        err_sys( "The number of ports exceeds the maximum number allowed. Exit..." );
    }
    *portNumber = *portNumber + 1;
}

